package rmiserver;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String name,email,googleID;
	private String username,password;
	private ArrayList <String> notifications = new ArrayList<String>();
	
	User(int id,String name, String email, String username,String password) {
		this.id=id;
		this.name=name;
		this.email=email;
		this.username=username;
		this.password=password;
		this.googleID="";
	}
	
	public User(int id,String name, String username,String googleID) {
		this.email =googleID+"@gmail.com";
		this.id=id;
		this.name=name;
		this.username=username;
		this.googleID = googleID;
	}

	
	public int getId() {
		return id;
	}
	
	public String getGoogleId() {
		return googleID;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public ArrayList<String> getNotifications() {
		return notifications;
	}

	public String printUser() {
		return this.id+" ->" +this.username+" (" + this.name + ", " + this.email + ")\n";
	}

	
	public void addNotification(String notif) {
		
		this.notifications.add(notif);
	}


	public String printNotifications() {
		String notif="";
		
		for (int i = 0; i < this.notifications.size(); i++)
			notif+=this.notifications.get(i);
		
		this.notifications.clear();	
		
		return notif;
	}


}
