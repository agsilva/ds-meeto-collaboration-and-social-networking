package rmiserver;


import java.io.Serializable;
import java.util.ArrayList;

public class Item implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private int meetID;
	private String itemName;
	private String username; 
	private ArrayList <String> chat = new ArrayList<String>();
	private ArrayList <String> decisions = new ArrayList<String>();
	
	Item(int meetID, int id, String itemName, String username) {
		super();
		this.meetID=meetID;
		this.id=id;
		this.itemName = itemName;
		this.username = username;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemName() {
		return itemName;
	}
	
	public String printItem()
	{
		return id+" -> [Added by "+username+"] "+this.itemName+"\n";
	}
	
	public String printDecisions()
	{
		String keys="";
		
		keys+="["+this.itemName+"] Key Decisions\n";
		
		if (this.decisions.isEmpty())
			return keys+"\nNothig to display";
		
		for (String k: this.decisions)
			keys+=k+"\n";
		
		return keys;
	}
	
	public String printChat()
	{
		String listChat="["+this.itemName+"] Chatroom\n";	
		
		if (this.chat.isEmpty())
			return listChat+"\nNothing to display";
		
		for (String c: this.chat)
			listChat+=c+"\n";
		
		return listChat;
	}

	
	public ArrayList<String> getChat() {
		return chat;
	}
	

	public void addChatLine(String chatLine) {
		this.chat.add(chatLine);
	}
	
	public void addDecision (String decisionLine)
	{
		this.decisions.add(decisionLine);
	}
	
	public int getMeetID() {
		return meetID;
	}

	public int getId() {
		return id;
	}
	
}
