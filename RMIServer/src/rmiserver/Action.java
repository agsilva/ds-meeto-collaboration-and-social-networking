package rmiserver;

import java.io.Serializable;

public class Action implements Serializable{
	private static final long serialVersionUID = 1L;
	private int itemID;
	private int userID;
	private int id;
	private String meetingName;
	private String itemName;
	private String username;
	private String task;
	private boolean check=false;
	
	Action(int itemID, int id, String meetingName, String itemName, int userID, String username, String task) {
		super();
		this.itemID=itemID;
		this.id=id;
		this.meetingName=meetingName;
		this.itemName=itemName;
		this.userID=userID;
		this.username = username;
		this.task = task;
		this.check = false;
	}
	
	public void setActionDone()
	{
		this.check=true;
	}
	
	public String printAllAction()
	{
		if (this.check)
			return "[Meeting: "+ this.meetingName+", Item: "+this.itemName+"] To "+this.username+": "+this.task+" -> DONE\n";
		
		return "[Meeting: "+ this.meetingName+", Item: "+this.itemName+"] To "+this.username+": "+this.task+" -> NOT DONE\n";
	}
	
	public String printUserAction()
	{
		if (this.check)
			return this.id+" -> [Meeting: "+ this.meetingName+", Item: "+this.itemName+"] "+this.task+" -> DONE\n";
		
		return this.id+" -> [Meeting: "+ this.meetingName+", Item: "+this.itemName+"] "+this.task+" -> NOT DONE\n";
	}
	

	public int getItemID() {
		return itemID;
	}

	public int getUserID() {
		return userID;
	}

	public String getUsername()
	{
		return this.username;
	}

	public int getId() {
		return id;
	}

	public boolean getActionDone() {
		return this.check;
	}
}
