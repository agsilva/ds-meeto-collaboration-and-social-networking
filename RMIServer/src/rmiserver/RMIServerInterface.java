package rmiserver;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;

import org.json.*;
import org.scribe.model.Token;

public interface RMIServerInterface extends Remote {
    public String registerUser (String nome, String email, String username, String password) throws RemoteException;
    public String loginUser (String username, String password) throws RemoteException;
	public String becomeOnline(int userID) throws RemoteException;	
	public String becomeOffline(int userID) throws RemoteException;
	public void print_on_RMIServer(String s) throws RemoteException;
	public String addMeeting(String title, String outcome, Date startDate, Date endDate, String location, int userID) throws RemoteException, InterruptedException;
    public String addItem(int meetID, String itemName, int userID) throws RemoteException;    
    public String removeItem(int itemID) throws RemoteException;
    public String modifyItem(int itemID, String itemName) throws RemoteException;
	public String addUser2Meeting(int meetID, int user2addID) throws RemoteException;
	public String addAction2User(int itemID, int user2addID, String action) throws RemoteException;	
	public String addDecision2Item(int itemID, String decision, int userID) throws RemoteException;
	public String printUsers(int userID) throws RemoteException;
	public String printCurrentMeetings(int userID) throws RemoteException;		
	public String printCurrentMeeting(int meetID) throws RemoteException;
	public String printMeetingItems (int meetID) throws RemoteException; 
	public String printChat(int itemID) throws RemoteException;	
	public String printUserMeetings(int userID) throws RemoteException;
	public String printUpcomingMeetings(int userID) throws RemoteException;
	public String getMeetingCrono(int meetID) throws RemoteException;
	public String printUserActions(int userID) throws RemoteException;
	public String printActions(int itemID) throws RemoteException;
	public String printDecisions(int itemID) throws RemoteException;
	public String send2chat(int meetID,int itemID, Date now, int userID, String line) throws RemoteException, InterruptedException;
	public String getUserStatus(int meetID, int userID) throws RemoteException; 
	public String actionDone(int actionID) throws RemoteException;
	public String printOnlineUsers() throws RemoteException;
	public String acceptDeclineMeeting(int meetID, int userID, int option) throws RemoteException;
	
	/* Groups */
	public String createGroup (String name, int userID) throws RemoteException;
	public String addGroup2Meeting(int meetID, int group2addID) throws RemoteException;
	public String addUser2Group(int groupID, int user2addID) throws RemoteException;
	public String addAction2Group(int itemID, int userID, int group2addID, String action) throws RemoteException;
	public String acceptDeclineGroup(int groupID, int userID, int option) throws RemoteException;
	public String printAllGroups() throws RemoteException;
	public String printUserGroups(int userID) throws RemoteException;
	public String printDetailedGroup(int groupID) throws RemoteException;
	
	//Project v2 Methods
	public String printMeetingUsers(int meetID) throws RemoteException;
	public int getuserID(String username) throws RemoteException;
	public ArrayList<String> getOnlineUsernames() throws RemoteException;
	public ArrayList<String> getUsersIDs() throws RemoteException;
	public ArrayList<String> getAllUsers() throws RemoteException;
	public void addWebChatLine(String line, int itemID) throws RemoteException;
	public ArrayList<String> getItemActions(int itemID) throws RemoteException;
	public int loginGoogle(String username,String googleID) throws RemoteException;
	public String getUserGoogleID(int userID) throws RemoteException;
	public void addMeetingOnGoogle(int meetID, Token accessToken) throws RemoteException;
	public int userOnMeeting(int userID,int meetID) throws RemoteException;
	public void updateMeetingOnGoogle(Token token,String itemName,int idItem,boolean whatToDo) throws RemoteException;

}


