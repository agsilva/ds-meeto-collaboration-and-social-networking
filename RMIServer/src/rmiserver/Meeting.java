package rmiserver;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;


public class Meeting implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String leader;
	private String title;
	private String outcome;
	private Date startTime;
	private Date endTime;
	private String location;
	private HashMap <Integer,Integer> addedUsers = new HashMap<Integer,Integer>();
	

	Meeting(int id, String title, String outcome, Date startTime, Date endTime, String location, int userID, String leader) {
		super();
		this.id=id;
		this.title = title;
		this.outcome = outcome;
		this.startTime = startTime;
		this.endTime = endTime;
		this.location = location;
		this.addedUsers.put(userID, 1);//1 -> already accepted the invite, because he's the leader
		this.leader=leader;
		
	}
	
	public String getTitle() {
		return title;
	}
	
	public int getId() {
		return id;
	}
	
	
	
	public String getLeader() {
		return leader;
	}

	public void setLeader(String leader) {
		this.leader = leader;
	}

	public String getOutcome() {
		return outcome;
	}

	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

	public Date getBeginTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public String getLocal() {
		return location;
	}

	
	public HashMap<Integer,Integer> getAddedUsers() {
		return addedUsers;
	}

	public String printMeeting() 
	{
 		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
 		String start = sdf.format(startTime);
 		String end = sdf.format(endTime);

		return this.id + " -> Meeting [Title: " + this.title + ", Outcome: " + this.outcome + ", Start Time: " + start + ", End Time: " + end + ", Local: " + this.location + "]\n";
	}
	
	public String printEnteredMeeting()
	{
 		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

 		String start = sdf.format(startTime);
 		String end = sdf.format(endTime);
		
 		return "Title: " + this.title + "\nOutcome: " + this.outcome + "\nStart Time: " + start + "\nEnd Time: " + end + "\nLocal: " + this.location+"\nLeader: "+this.leader+"\n\n**Users**";
	}


	
	public boolean acceptedMeeting (int userID)
	{		
		for (Entry<Integer, Integer> entry : addedUsers.entrySet()) 
		    if(userID == entry.getKey())
		    {
		    	if (entry.getValue()==1)
		    		return true;
		    	
		    	return false;
		    }
		    
		return false;
	}
	
	public int userStatus (int userID)
	{		
		for (Entry<Integer, Integer> entry : addedUsers.entrySet()) 
		    if(userID == entry.getKey())
		    	return entry.getValue();
		    
		    
		return 2;
	}
	
	
	public void declineMeeting (int userID)
	{
		for (Entry<Integer, Integer> entry : addedUsers.entrySet()) 
		    if(userID == entry.getKey()){
		    	entry.setValue(-1);
		    	break;
		    }
	}
	
	public void acceptMeeting (int userID)
	{
		for (Entry<Integer, Integer> entry : addedUsers.entrySet()) 
		    if(userID == entry.getKey()){
		    	entry.setValue(1);
		    	break;
		    }
	}

	
}
