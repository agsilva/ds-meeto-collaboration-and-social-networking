package rmiserver;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;

public class Group implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String name,creator;
	private HashMap <Integer, Integer> users = new HashMap<Integer,Integer>();

	Group(int id, String name, int userID, String creator) {
		this.id = id;
		this.name = name;
		this.users.put(userID, 1);
		this.creator = creator;
	}
	
	public String printGroup() 
	{
		return this.id + " -> Group [Name: " + this.name + ", Creator: "+this.creator+"]\n";
	}
	
	
	public String printEnteredGroup()
	{	
 		return "Name: " + this.name + "\nCreator: " + this.creator + "\n\n**Users**\n";
	}

	
	public HashMap<Integer, Integer> getUsers() {
		return this.users;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}

	
	public void declineGroup (int userID)
	{
		users.remove(userID);
	}
	
	public void acceptGroup(int userID)
	{
		for (Entry<Integer, Integer> entry : users.entrySet()) 
		    if(userID == entry.getKey()){
		    	entry.setValue(1);
		    	break;
		    }
	}
	
	public int userStatus (int userID)
	{		
		for (Entry<Integer, Integer> entry : users.entrySet()) 
		    if(userID == entry.getKey())
		    	return entry.getValue();
		    
		    
		return 2;
	}
	
	

}
