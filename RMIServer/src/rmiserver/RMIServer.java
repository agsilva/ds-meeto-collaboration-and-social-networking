package rmiserver;

import rmiserver.Action;
import rmiserver.Group;
import rmiserver.InterfaceClient;
import rmiserver.Item;
import rmiserver.Meeting;
import rmiserver.RMIServer;
import rmiserver.RMIServerInterface;
import rmiserver.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.rmi.AccessException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.GoogleApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

public class RMIServer extends UnicastRemoteObject implements
RMIServerInterface {
	private static final long serialVersionUID = 1L;
	private static ArrayList<User> users = new ArrayList<User>();
	private static ArrayList<User> onlineUsers = new ArrayList<User>();
	private static ArrayList<Meeting> meetings = new ArrayList<Meeting>();
	public static ArrayList<String> serverAddress = new ArrayList<String>();
	private static ArrayList<Item> items = new ArrayList<Item>();
	private static ArrayList<Action> actions = new ArrayList<Action>();
	private static ArrayList<Group> groups = new ArrayList<Group>();


	private static final String NETWORK_NAME = "Google";
	private static final String AUTHORIZE_URL = "https://www.google.com/accounts/OAuthAuthorizeToken?oauth_token=";
	private static final String PROTECTED_RESOURCE_URL = "https://www.googleapis.com/calendar/v3/calendars/primary/events/";
	private static final String SCOPE = "https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/calendar";
	private static final Token EMPTY_TOKEN = null;
	private static final String CALLBACK_URL = "http://localhost:8082/hey/google";


	private static int nrUsers = 0;
	private static int nrMeetings = 0;
	private static int nrActions = 0;
	private static int nrGroups = 0;

	static InterfaceClient client;

	protected RMIServer() throws RemoteException {
		super();
	}

	public void print_on_RMIServer(String s) throws RemoteException {
		System.out.println("> " + s);
	}

	public String becomeOnline(int userID) throws RemoteException {
		synchronized (onlineUsers) {
			String notif;
			User u = findUser(userID);

			if (!onlineUsers.contains(u))
				onlineUsers.add(u);

			if (u.getNotifications().size() != 0) {
				notif = "\n**Notifications**\n" + u.printNotifications();
				// for(int i = 0; i < u.getNotifications().size(); i++)
				// u.getNotifications().remove(i);
				writeUserFile();

				return "print#" + notif;
			}

			return "ok#";

		}
	}

	public String becomeOffline(int userID) throws RemoteException {
		synchronized (onlineUsers) {
			User u = findUser(userID);
			onlineUsers.remove(u);
			return "ok#";
		}
	}

	public static void main(String[] args) throws AccessException,
	RemoteException {
		loadProperties();
		System.setProperty("java.rmi.server.hostname", serverAddress.get(0));
		RMIServerInterface s = new RMIServer();
		LocateRegistry.createRegistry(1099).rebind("server", s);
		System.out.println("Server ready...");

		readUserFile();
		readMeetingFile();
		readItemFile();
		readActionFile();
		readGroupFile();
	}

	/**********************************************************************************************/
	/**************************** LOAD FILES ************************************************/
	/********************************************************************************************/
	private static void readUserFile() {

		synchronized (users) {
			FileInputStream fileIn;
			ObjectInputStream in;
			Object obj;

			File file = new File("Users.obj");
			if (file.exists()) {
				User u = null;
				try {
					fileIn = new FileInputStream("Users.obj");

					try {
						in = new ObjectInputStream(fileIn);

						while ((obj = in.readObject()) != null) {
							u = (User) obj;
							users.add(u);
							nrUsers++;
						}
						in.close();
						fileIn.close();

					} catch (IOException e) {

					} catch (ClassNotFoundException e) {
					}

				} catch (FileNotFoundException e) {
					System.out.println("FileInputStream not found");
				}

			}
			return;
		}
	}

	private static void readMeetingFile() {

		synchronized (meetings) {

			FileInputStream fileIn;
			ObjectInputStream in;
			Object obj = null;

			File file = new File("Meetings.obj");
			if (file.exists()) {
				Meeting m = null;
				try {
					fileIn = new FileInputStream("Meetings.obj");

					try {
						in = new ObjectInputStream(fileIn);

						while ((obj = in.readObject()) != null) {
							m = (Meeting) obj;
							meetings.add(m);
							nrMeetings++;
						}

						in.close();
						fileIn.close();

					} catch (IOException e) {

					} catch (ClassNotFoundException e) {
					}

				} catch (FileNotFoundException e) {
					System.out.println("FileInputStream not found");
				}

			}
			return;
		}
	}

	private static void loadProperties() {

		InputStream in = null;
		try {
			Properties properties = new Properties();
			in = new FileInputStream("app.properties");
			properties.load(in);

			serverAddress.add(properties.getProperty("server1.address"));
			serverAddress.add(properties.getProperty("server2.address"));
			System.out.println("server address1: " + serverAddress.get(0));
			System.out.println("server address2: " + serverAddress.get(1));

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Something went wrong:  loadProperties(): " + e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}

	}

	private static void readItemFile() {

		synchronized (items) {

			FileInputStream fileIn;
			ObjectInputStream in;
			Object obj = null;

			File file = new File("Items.obj");
			if (file.exists()) {
				Item i = null;
				try {
					fileIn = new FileInputStream("Items.obj");

					try {
						in = new ObjectInputStream(fileIn);

						while ((obj = in.readObject()) != null) {
							i = (Item) obj;
							items.add(i);
						}

						in.close();
						fileIn.close();

					} catch (IOException e) {

					} catch (ClassNotFoundException e) {
					}

				} catch (FileNotFoundException e) {
					System.out.println("FileInputStream not found");
				}

			}
			return;
		}
	}

	private static void readActionFile() {

		synchronized (actions) {

			FileInputStream fileIn;
			ObjectInputStream in;
			Object obj = null;

			File file = new File("Actions.obj");
			if (file.exists()) {
				Action a = null;
				try {
					fileIn = new FileInputStream("Actions.obj");

					try {
						in = new ObjectInputStream(fileIn);

						while ((obj = in.readObject()) != null) {
							a = (Action) obj;
							actions.add(a);
							nrActions++;
						}

						in.close();
						fileIn.close();

					} catch (IOException e) {
					} catch (ClassNotFoundException e) {
					}

				} catch (FileNotFoundException e) {
					System.out.println("FileInputStream not found");
				}

			}
			return;
		}
	}

	private static void readGroupFile() {

		synchronized (groups) {

			FileInputStream fileIn;
			ObjectInputStream in;
			Object obj = null;

			File file = new File("Groups.obj");
			if (file.exists()) {
				Group g = null;
				try {
					fileIn = new FileInputStream("Groups.obj");

					try {
						in = new ObjectInputStream(fileIn);

						while ((obj = in.readObject()) != null) {
							g = (Group) obj;
							groups.add(g);
							nrGroups++;
						}

						in.close();
						fileIn.close();

					} catch (IOException e) {
					} catch (ClassNotFoundException e) {
					}

				} catch (FileNotFoundException e) {
					System.out.println("FileInputStream not found");
				}

			}
			return;
		}
	}

	/*********************************************************************************************/
	/**************************** SAVE/UPDATE FILES ***************************************/
	/*********************************************************************************************/

	private static void writeUserFile() {

		synchronized (users) {

			FileOutputStream fileOut = null;
			ObjectOutputStream out;

			try {
				fileOut = new FileOutputStream("Users.obj");

				try {
					out = new ObjectOutputStream(fileOut);

					for (User u : users)
						out.writeObject(u);

					out.close();

				} catch (IOException e1) {
					System.out.println("ObjectOutputStream not found");
				}

				fileOut.close();

			} catch (IOException e1) {
				System.out.println("FileOutputStream not found");

			}
			return;

		}
	}

	private static void writeMeetingFile() {

		synchronized (meetings) {

			FileOutputStream fileOut = null;
			ObjectOutputStream out;

			try {
				fileOut = new FileOutputStream("Meetings.obj");

				try {
					out = new ObjectOutputStream(fileOut);

					for (Meeting m : meetings)
						out.writeObject(m);

					out.close();

				} catch (IOException e1) {
					System.out.println("ObjectOutputStream not found");
				}

				fileOut.close();

			} catch (IOException e1) {
				System.out.println("FileOutputStream not found");

			}
			return;

		}
	}

	private static void writeItemFile() {

		synchronized (items) {

			FileOutputStream fileOut = null;
			ObjectOutputStream out;

			try {
				fileOut = new FileOutputStream("Items.obj");

				try {
					out = new ObjectOutputStream(fileOut);

					for (Item i : items)
						out.writeObject(i);

					out.close();

				} catch (IOException e1) {
					System.out.println("ObjectOutputStream not found");
				}

				fileOut.close();

			} catch (IOException e1) {
				System.out.println("FileOutputStream not found");

			}
			return;

		}
	}

	private static void writeActionFile() {

		synchronized (actions) {

			FileOutputStream fileOut = null;
			ObjectOutputStream out;

			try {
				fileOut = new FileOutputStream("Actions.obj");

				try {
					out = new ObjectOutputStream(fileOut);

					for (Action a : actions)
						out.writeObject(a);

					out.close();

				} catch (IOException e1) {
					System.out.println("ObjectOutputStream not found");
				}

				fileOut.close();

			} catch (IOException e1) {
				System.out.println("FileOutputStream not found");

			}
			return;

		}
	}

	private static void writeGroupFile() {
		synchronized (groups) {

			FileOutputStream fileOut = null;
			ObjectOutputStream out;

			try {
				fileOut = new FileOutputStream("Groups.obj");

				try {
					out = new ObjectOutputStream(fileOut);

					for (Group g : groups)
						out.writeObject(g);

					out.close();

				} catch (IOException e1) {
					System.out.println("ObjectOutputStream not found");
				}

				fileOut.close();

			} catch (IOException e1) {
				System.out.println("FileOutputStream not found");

			}
			return;

		}
	}

	/*********************************************************************************************/
	/**************************** CREDENTIALS *********************************************/
	/*********************************************************************************************/

	public String registerUser(String name, String email, String username,
			String password) throws RemoteException {
		synchronized (users) {

			if (!checkEmail(email)) {
				if (!checkUsername(username)) {
					nrUsers++;
					User u = new User(nrUsers, name, email.toLowerCase(),
							username, password);
					users.add(u);

					writeUserFile();
					System.out.println("[REGISTER] File 'Users.obj' updated");
					return "ok";
				} else {
					System.out
					.println("[REGISTER] Error: Username already in use");
					return "err#Error: Username already in use";
				}

			}

			System.out.println("[REGISTER] Error: Email already in use");
			return "err#Error: Email already in use";
		}

	}

	public String loginUser(String username, String password)
			throws RemoteException {

		synchronized (users) {
			User u;

			if ((u = findUsername(username)) == null) {
				System.out.println("[LOGIN] Error: Username not found");
				return "err#Error: Username not found";
			}

			if (u.getPassword().equals(password)) {
				if (!checkOnlineUser(u)) { // if user's offline (not logged in)
					System.out.println("User " + u.getUsername()
							+ " is now online");
					return "ok#" + u.getId();
				}

				System.out.println("[LOGIN] Error: " + username
						+ " already online.");
				return "err#Error: " + username + " already online.";
			}

			System.out
			.println("[LOGIN] Error: Password doesn't match username's.");
			return "err#Error: Password doesn't match username's.";
		}

	}

	public String createGroup(String name, int userID) throws RemoteException {
		User u;
		Group g;

		synchronized (groups) {

			u = findUser(userID);
			if (u == null)
				return "err#Inexistant ID";
			nrGroups++;
			g = new Group(nrGroups, name, userID, u.getUsername());
			groups.add(g);

			writeGroupFile();

			System.out.println("[GROUP] Group created succesfully.");
			return "ok#Group created succesfully.#" + nrGroups; // returns
			// groupID
		}

	}

	/*********************************************************************************************/
	/**************************** ADDS ****************************************************/
	/*********************************************************************************************/

	public String addMeeting(String title, String outcome, Date startDate,
			Date endDate, String location, int userID) throws RemoteException,

			InterruptedException {
		Meeting m1;
		int index = 0;
		int itemID;
		Item i;
		User u;

		synchronized (meetings) {

			if (!meetings.isEmpty()) {
				for (Meeting m : meetings) {
					index++;

					if (m.getBeginTime().after(endDate)) {
						index--;
						break;
					}

					if (m.getLocal().equals(location)) {
						if ((startDate.compareTo(m.getBeginTime()) >= 0 && startDate
								.compareTo(m.getEndTime()) <= 0)) {
							return "err#Error: Another meeting is already scheduled to that start date";
						}
						if (endDate.compareTo(m.getBeginTime()) >= 0
								&& endDate.compareTo(m.getEndTime()) <= 0) {
							return "err#Error: Another meeting is already scheduled to that end date";
						}
					}
				}

			}

			nrMeetings++;
			u = findUser(userID);

			m1 = new Meeting(nrMeetings, title, outcome, startDate, endDate,
					location, userID, u.getUsername());

			if (!items.isEmpty())
				itemID = items.get(items.size() - 1).getId() + 1;
			else
				itemID = 1;

			i = new Item(nrMeetings, itemID, "Any other business",
					u.getUsername());
			items.add(i);

			meetings.add(index, m1);

			writeMeetingFile();
			writeItemFile();

			// for (User us: users)
			// us.updateMeetingsIndex(index);

		}
		return "ok#Meeting scheduled succesfully.#" + nrMeetings; // returns
		// meetID
	}

	public String addItem(int meetID, String itemName, int userID)
			throws RemoteException {
		synchronized (items) {
			Item i;
			int itemID;
			User u = findUser(userID);

			if (findMeeting(meetID) == null)
				return "err#Inexistant ID";

			itemID = items.get(items.size() - 1).getId() + 1;
			i = new Item(meetID, itemID, itemName, u.getUsername());
			items.add(i);

			writeItemFile();

			return "ok#Item added sucessfully";
		}

	}
	
	
	public int userOnMeeting(int userID,int meetID){
		Meeting m = findMeeting(meetID);
		
		//return m.userStatus(userID);
		return 2;
	}
	
	

	public String removeItem(int itemID) throws RemoteException {
		synchronized (items) {
			Item i;

			i = findItem(itemID);
			if (i == null)
				return "err#Inexistant ID";

			if (i.getItemName().equals("Any other business"))
				return "err#You can't remove this item";

			items.remove(i);

			writeItemFile();

			return "ok#Item removed sucessfully";
		}
	}

	public String modifyItem(int itemID, String itemName)
			throws RemoteException {
		synchronized (items) {
			Item i;

			i = findItem(itemID);
			if (i == null)
				return "err#Inexistant ID";

			if (i.getItemName().equals("Any other business"))
				return "err#You can't modify this item";

			i.setItemName(itemName);

			writeItemFile();

			return "ok#Item modified sucessfully";
		}
	}

	/* CALLBACK */
	public String addUser2Meeting(int meetID, int user2addID)
			throws RemoteException {
		synchronized (users) {

			Meeting m;
			User u;

			m = findMeeting(meetID);
			if (m == null)
				return "err#Inexistant ID";

			u = findUser(user2addID);
			if (u == null)
				return "err#Inexistant ID";

			if (!m.getAddedUsers().containsKey(user2addID)) {
				m.getAddedUsers().put(user2addID, 0);

				writeMeetingFile();

				if (!checkOnlineUser(u)) {
					u.addNotification("You were invited to\n"
							+ m.printMeeting());
					writeUserFile();
				}
				// else
				// CALLBACK...

				return "print#" + u.getUsername() + " invited.";
			} else
				return "print#" + u.getUsername() + " already invited!";
		}
	}

	public String addGroup2Meeting(int meetID, int group2addID)
			throws RemoteException {
		synchronized (users) {

			Meeting m;
			Group g;
			int user2addID;
			Set<Integer> keys;

			m = findMeeting(meetID);
			if (m == null)
				return "err#Inexistant ID";

			g = findGroup(group2addID);
			if (g == null)
				return "err#Inexistant ID";

			keys = g.getUsers().keySet();

			for (Iterator<Integer> j = keys.iterator(); j.hasNext();) {
				user2addID = j.next();
				if (g.userStatus(user2addID) == 1)
					addUser2Meeting(meetID, user2addID);
			}

			writeMeetingFile();

			return "print#Group '" + g.getName() + "' invited.";
		}
	}

	public String addUser2Group(int groupID, int user2addID)
			throws RemoteException {
		synchronized (users) {

			Group g;
			User u;

			g = findGroup(groupID);
			if (g == null)
				return "err#Inexistant ID";

			u = findUser(user2addID);
			if (u == null)
				return "err#Inexistant ID";

			if (!g.getUsers().containsKey(user2addID)) {
				g.getUsers().put(user2addID, 0);

				writeGroupFile();

				if (!checkOnlineUser(u)) {
					u.addNotification("You were invited to\n" + g.printGroup());
					writeUserFile();
				}
				// else
				// CALLBACK...

				return "print#" + u.getUsername() + " invited.";
			} else
				return "print#" + u.getUsername() + " already invited!";
		}
	}

	/* CALLBACK */
	public String addAction2User(int itemID, int user2addID, String action)
			throws RemoteException {
		synchronized (users) {
			Action a;
			User u;
			Item i;
			Meeting m;

			i = findItem(itemID);
			if (i == null)
				return "err#Inexistant ID";
			m = findMeeting(i.getMeetID());
			if (m == null)
				return "err#Inexistant ID";
			u = findUser(user2addID);
			if (u == null)
				return "err#Inexistant ID";

			nrActions++;
			a = new Action(itemID, nrActions, m.getTitle(), i.getItemName(),
					user2addID, u.getUsername(), action);
			actions.add(a);

			writeActionFile();

			if (!checkOnlineUser(u)) {
				u.addNotification("You were given a task:\n" + m.printMeeting()
						+ i.printItem());
				writeUserFile();
			}

			// else
			// CALLBACK...

			return "print#Task assigned to " + u.getUsername()
					+ " successfully.";
		}
	}

	public String addAction2Group(int itemID, int userID, int group2addID,
			String action) throws RemoteException {
		synchronized (users) {
			Group g;
			Item i;
			Meeting m;
			Set<Integer> keys;
			int user2addID;

			i = findItem(itemID);
			if (i == null)
				return "err#Inexistant ID";
			m = findMeeting(i.getMeetID());
			if (m == null)
				return "err#Inexistant ID";
			g = findGroup(group2addID);
			if (g == null)
				return "err#Inexistant ID";

			keys = g.getUsers().keySet();

			for (Iterator<Integer> j = keys.iterator(); j.hasNext();) {
				user2addID = j.next();
				if (g.userStatus(user2addID) == 1)
					addAction2User(itemID, user2addID, action);
			}

			return "print#Task assigned to group '" + g.getName()
					+ "' successfully.";
		}
	}

	public String addDecision2Item(int itemID, String decision, int userID)
			throws RemoteException {
		synchronized (items) {
			User u = findUser(userID);
			Item i = findItem(itemID);

			if (i == null)
				return "err#Inexistant ID";

			i.addDecision("[" + u.getUsername() + "] " + decision);

			writeItemFile();

			return "print#Decision added sucessfully";
		}

	}

	/*********************************************************************************************/
	/**************************** PRINTS **************************************************/
	/*********************************************************************************************/

	public String printOnlineUsers() throws RemoteException {
		String online = "";

		for (User u : onlineUsers)
			online += u.getUsername() + "\n";

		return online;
	}

	public String printUsers(int userID) throws RemoteException {
		String listUsers = "";
		int len = 0;

		if (!users.isEmpty())
			len = users.size() - 1;

		listUsers = len + "\n";

		if (len != 0)
			for (User u : users)
				if (u.getId() != userID)
					listUsers += u.printUser();

		return "print#" + listUsers;

	}

	public String printAllGroups() throws RemoteException {
		String listGroups = "";
		int len = 0;

		if (!groups.isEmpty())
			len = groups.size() - 1;

		listGroups = len + "\n";

		if (len != 0)
			for (Group g : groups)
				listGroups += g.printGroup();

		return "print#" + listGroups;
	}

	public String printCurrentMeetings(int userID) throws RemoteException {
		String listMeetings = "";
		String listCurr = "";
		int id = 0;
		Date now = new Date();
		System.out.println("Now = " + now);

		if (!meetings.isEmpty()) {
			for (Meeting m : meetings) {
				if (m.getAddedUsers().containsKey(userID))
					if (m.getBeginTime().compareTo(now) <= 0
					&& m.getEndTime().compareTo(now) >= 0
					&& m.acceptedMeeting(userID)) {
						id++;
						listCurr += m.printMeeting();
					}

			}
		}

		listMeetings = id + "\n" + listCurr;

		return "print#" + listMeetings;
	}

	public String printCurrentMeeting(int meetID) throws RemoteException {
		Meeting m;

		m = findMeeting(meetID);
		if (m == null)
			return "err#Inexistant ID";

		return "print#" + m.printEnteredMeeting() + printMeetingUsers(meetID);
	}

	public String printDetailedGroup(int groupID) throws RemoteException {
		Group g;

		g = findGroup(groupID);
		if (g == null)
			return "err#Inexistant ID";

		return "print#" + g.printEnteredGroup() + printGroupUsers(groupID);
	}

	public String printMeetingItems(int meetID) throws RemoteException {
		String listItems = "";
		String last = "";
		String listCurr = "";
		int id = 0;
		int counter = 0;

		if (findMeeting(meetID) == null)
			return "err#Inexistant ID";

		for (Item i : items)
			if (i.getMeetID() == meetID) {
				if (counter == 0) {
					counter++;
					last += i.printItem();
				} else {
					id++;
					listCurr += id + ": " + i.printItem();
				}
			}

		if (counter != 0)
			id++;

		listItems = id + "\n" + listCurr + id + ": " + last;

		return "print#" + listItems;
	}

	public String printChat(int itemID) throws RemoteException {
		Item i = findItem(itemID);

		if (i == null)
			return "err#Inexistant ID";

		return "print#" + i.printChat();

	}

	public String printUserMeetings(int userID) throws RemoteException {
		String listCurr = "";
		String listMeetings = "";
		int id = 0;

		for (Meeting m : meetings)
			if (m.getAddedUsers().containsKey(userID)) {
				id++;
				listCurr += getStatus(m, userID) + m.printMeeting();
			}

		listMeetings = id + "\n" + listCurr;

		return "print#" + listMeetings;
	}

	public String printUpcomingMeetings(int userID) throws RemoteException {
		String listCurr = "";
		String listMeetings = "";
		Date now = new Date();
		int id = 0;

		for (Meeting m : meetings)
			if (m.getAddedUsers().containsKey(userID)
					&& m.getBeginTime().after(now)) {
				id++;
				listCurr += getStatus(m, userID) + m.printMeeting();
			}

		listMeetings = id + "\n" + listCurr;

		return "print#" + listMeetings;
	}

	public String printUserGroups(int userID) throws RemoteException {
		String listCurr = "";
		String listGroups = "";
		int id = 0;

		for (Group g : groups)
			if (g.getUsers().containsKey(userID)) {
				id++;
				listCurr += getUserGroupStatus(g, userID) + g.printGroup();
			}

		listGroups = id + "\n" + listCurr;

		return "print#" + listGroups;
	}

	public String getMeetingCrono(int meetID) throws RemoteException {
		Date now = new Date();
		int crono;
		Meeting m = findMeeting(meetID);

		if (m == null)
			return "err#InexistantID";

		if (m.getEndTime().before(now))
			crono = -1; // past meetings

		else if (m.getBeginTime().before(now) && m.getEndTime().after(now))
			crono = 0; // current meetings

		else
			crono = 1;// future meetings

		return "print#" + crono;
	}

	public String printUserActions(int userID) throws RemoteException {

		String listActions = "";
		String listCurr = "";
		int id = 0;

		if (actions.isEmpty())
			listActions += "0\n";

		else {
			for (Action a : actions)
				if (a.getUserID() == userID) {
					id++;
					listCurr += a.printUserAction();
				}
			listActions = id + "\n" + listCurr;
		}

		return "print#" + listActions;
	}

	public String printActions(int itemID) throws RemoteException {

		String listCurr = "";
		Item i;
		String listActions = "";

		i = findItem(itemID);

		if (i == null)
			return "err#InexistantID";

		listActions += "[" + i.getItemName() + "] Actions\n";

		if (actions.isEmpty())
			listActions += "\nNothing to display";

		else {
			for (Action a : actions)
				if (a.getItemID() == itemID)
					listCurr += a.printAllAction();
		}

		if (!listCurr.isEmpty())
			listActions += listCurr;

		return "print#" + listActions;
	}

	public String printDecisions(int itemID) throws RemoteException {
		Item i = findItem(itemID);

		if (i == null)
			return "err#Inexistant ID";

		return "print#" + i.printDecisions();
	}

	public String printMeetingUsers(int meetID) {
		Meeting m;
		String listUsers = "";
		Integer value;
		User u;

		listUsers = "";
		m = findMeeting(meetID);
		if (m == null)
			return "err#Inexistant ID";

		for (Entry<Integer, Integer> entry : m.getAddedUsers().entrySet()) {
			u = findUser(entry.getKey());
			value = entry.getValue();
			if (value == 1)
				listUsers += "[Accepted] ";
			else {
				if (value == 0)
					listUsers += "[Invited] ";
				else
					listUsers += "[Declined] ";
			}
			listUsers += u.getUsername() + "\n";

		}
		System.out.println("returning meeting users...");
		return "print#" + listUsers;
	}

	private String printGroupUsers(int groupID) {
		Group g;
		String listUsers = "";
		Integer value;
		User u;

		listUsers = "";
		g = findGroup(groupID);
		if (g == null)
			return "err#Inexistant ID";

		for (Entry<Integer, Integer> entry : g.getUsers().entrySet()) {
			u = findUser(entry.getKey());
			value = entry.getValue();
			if (value == 1)
				listUsers += "[Accepted] ";
			else {
				if (value == 0)
					listUsers += "[Invited] ";
				else
					listUsers += "[Declined] ";
			}
			listUsers += u.getUsername() + "\n";

		}
		return listUsers;
	}

	/*********************************************************************************************/
	/**************************** RESPONSES ***********************************************/
	/*********************************************************************************************/

	public String send2chat(int meetID, int itemID, Date now, int userID,
			String line) throws RemoteException, InterruptedException {
		synchronized (items) {

			String chatLine = "";
			String date;
			int len = 0;
			Meeting m;
			Item i;
			User u;

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");// no
			// caso
			// de
			// a
			// reuniao
			// durar
			// mais
			// do
			// que
			// 1
			// dia

			m = findMeeting(meetID);
			i = findItem(itemID);
			u = findUser(userID);

			if (m.getEndTime().compareTo(now) >= 0) {
				date = sdf.format(now);
				chatLine += "[" + date + "] " + u.getUsername() + ": " + line;
				System.out.println("Estou no item " + i.getItemName());
				;
				i.addChatLine(chatLine);

				len = i.getChat().size();
				System.out.println(i.getChat().get(len - 1));

				writeItemFile();
				return "ok#Message added.";
			}

			else {
				return "err_chat#"
						+ "Meeting already over. Type /quit to return";
			}
		}

	}

	public String getUserStatus(int meetID, int userID) throws RemoteException {
		Meeting m = findMeeting(meetID);

		return "print#" + m.userStatus(userID);
	}

	public String actionDone(int actionID) throws RemoteException {
		Action a;

		a = findAction(actionID);
		if (a == null)
			return "print#Inexistant ID";

		if (a.getActionDone())
			return "print#Action already done";

		a.setActionDone();
		writeActionFile();

		return "print#Action marked as done sucessfully";

	}

	public String getStatus(Meeting m, int userID) {
		Integer value;

		value = m.userStatus(userID);
		if (value == 1)
			return "[Accepted] ";
		else {
			if (value == 0)
				return "[Invited] ";
			else
				return "[Declined] ";
		}
	}

	public String acceptDeclineMeeting(int meetID, int userID, int option)
			throws RemoteException {
		Meeting m = findMeeting(meetID);

		if (m == null)
			return "print#Inexistant ID";

		if (!m.getAddedUsers().containsKey(userID))
			return "print#Inexistant ID";

		if (option == 1)
			m.acceptMeeting(userID);

		if (option == 2)
			m.declineMeeting(userID);

		writeMeetingFile();

		return "ok#Operation done successfully";

	}

	public String acceptDeclineGroup(int groupID, int userID, int option)
			throws RemoteException {
		Group g = findGroup(groupID);

		if (g == null)
			return "print#Inexistant ID";

		if (!g.getUsers().containsKey(userID))
			return "print#Inexistant ID";

		if (option == 1)
			g.acceptGroup(userID);

		if (option == 2)
			g.declineGroup(userID);

		writeGroupFile();

		return "ok#Operation done successfully";

	}

	/**********************************************************************************************/
	/**************************** PROJECT V2.0 UPDATES ************************************************/
	/********************************************************************************************/

	public String checkUserAction(int meetID, int itemID, int actionID) {
		return null;
	}

	public int getuserID(String username) throws RemoteException {
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getUsername().equals(username)) {
				return users.get(i).getId();
			}
		}

		return 0;
	}

	public ArrayList<String> getOnlineUsernames() throws RemoteException {

		ArrayList<String> usersOnline = new ArrayList<String>();

		for (int i = 0; i < onlineUsers.size(); i++) {
			usersOnline.add(onlineUsers.get(i).getUsername());
		}

		return usersOnline;
	}

	public ArrayList<String> getAllUsers() throws RemoteException {
		ArrayList<String> allUserNames = new ArrayList<String>();

		for (int i = 0; i < users.size(); i++) {
			allUserNames.add(users.get(i).getUsername());
		}

		return allUserNames;
	}

	public ArrayList<String> getUsersIDs() throws RemoteException {
		String temp;
		ArrayList<String> usersAndID = new ArrayList<String>();
		System.out.println("recebi o pedido getAllUsersID");
		for (int i = 0; i < users.size(); i++) {

			temp = (Integer.toString(users.get(i).getId()) + " - " + users.get(
					i).getUsername());
			System.out.println("vou adicionar ao array: " + temp);
			usersAndID.add(temp);
		}

		return usersAndID;
	}

	public void addWebChatLine(String line, int itemID) throws RemoteException {
		Item i = findItem(itemID);
		i.addChatLine(line);

		writeItemFile();
	}

	public ArrayList<String> getItemActions(int itemID) throws RemoteException {

		ArrayList<String> itemActions = new ArrayList<String>();

		for (int i = 0; i < actions.size(); i++) {
			if (actions.get(i).getItemID() == itemID) {
				itemActions.add(actions.get(i).toString());
			}
		}

		return itemActions;
	}

	public int registerGoogle(String name, String username, String googleID) {

		synchronized (users) {

			nrUsers++;
			User u = new User(nrUsers, name, username, googleID);
			users.add(u);

			writeUserFile();
			System.out.println("[REGISTER] File 'Users.obj' updated");
			return nrUsers;

		}

	}

	public int loginGoogle(String username, String googleID)
			throws RemoteException {
		synchronized (users) {

			System.out.println("NHE RMI");
			// Se existir caga
			// senao existir -> faz registo

			for (int i = 0; i < users.size(); i++) {
				if (!users.get(i).getGoogleId().equals("")) {
					System.out.println("entrei aqui");
					if (users.get(i).getGoogleId().equals(googleID)) {
						becomeOnline(users.get(i).getId());
						return users.get(i).getId(); // Devolve o user ID deste
						// user Google
					}
				}

			}

			int id = registerGoogle(username, username, googleID);
			becomeOnline(id);
			return id;// Devolve o user ID deste NOVO user Google

		}

	}

	/*********************************************************************************************/
	/******************************** AUXILIARY METHODS ******************************************/
	/*********************************************************************************************/

	public String getUserGoogleID(int userID) {

		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getId() == userID) {
				return users.get(i).getGoogleId();
			}
		}

		return "";
	}

	private boolean checkEmail(String email) {
		for (User u : users) {
			if (u.getEmail().equalsIgnoreCase(email))
				return true;
		}
		return false;
	}

	private boolean checkUsername(String username) {
		for (User u : users) {
			if (u.getGoogleId().isEmpty())
				if (u.getUsername().equalsIgnoreCase(username))
					return true;
		}
		return false;
	}

	private boolean checkOnlineUser(User u) {

		return onlineUsers.contains(u);
	}

	private User findUser(int userID) {
		for (User u : users)
			if (u.getId() == userID)
				return u;

		return null;
	}

	private User findUsername(String username) {
		for (User u : users)
			if (u.getUsername().equalsIgnoreCase(username))
				return u;

		return null;
	}

	private Meeting findMeeting(int meetID) {
		for (Meeting m : meetings)
			if (m.getId() == meetID)
				return m;

		return null;
	}

	private Item findItem(int itemID) {
		for (Item i : items)
			if (i.getId() == itemID)
				return i;

		return null;
	}

	private Action findAction(int actionID) {
		for (Action a : actions)
			if (a.getId() == actionID)
				return a;

		return null;
	}

	private Group findGroup(int groupID) {
		for (Group g : groups)
			if (g.getId() == groupID)
				return g;

		return null;
	}

	private String getUserGroupStatus(Group g, int userID) {
		Integer value;

		value = g.userStatus(userID);
		if (value == 1)
			return "[Accepted] ";
		else {
			if (value == 0)
				return "[Invited] ";
			else
				return "[Declined] ";
		}
	}
	
	
	private OAuthService callService() {
		OAuthService service = new ServiceBuilder()
				.provider(GoogleApi.class)
				.apiKey("464058319942-qsttnc2t2srj73n2atdlkq3v1gqkepch.apps.googleusercontent.com")
				.apiSecret("QVeN2o4DGgK8AHfSyY6ElX_u").callback(CALLBACK_URL)
				.scope(SCOPE).build();

		return service;
	}
	
	
	





	public void addMeetingOnGoogle(int meetID, Token accessToken) {
		// TODO
		Meeting m = findMeeting(meetID);
		System.out.println("meeting ID: "+meetID);
		System.out.println(m.toString());
		OAuthService service = callService();
		
		OAuthRequest request = new OAuthRequest(Verb.POST,
				"https://www.googleapis.com/calendar/v3/calendars/primary/events");


		String eventName = m.getTitle(); 
		String eventDescription = m.getOutcome();
		String eventLocation = m.getLocal(); 
		String eventOrganizer = m.getLeader();
		
		Date eventStartTime = m.getBeginTime(); //must be STRING, not GregorianCalendar
		Date eventEndTime = m.getEndTime();
		String start = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(eventStartTime);
		String end = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(eventEndTime);

		JSONObject jBody = new JSONObject(); 
		jBody.put("colorId", "6");
		jBody.put("summary",eventName); 
		jBody.put("description", eventDescription);
		jBody.put("location", eventLocation);
		
		
		JSONArray startTimeList = new JSONArray(); 
		JSONObject content = new JSONObject(); 
		content.put("dateTime",start); // Integer.parseInt(meetingHour);
		content.put("timeZone", "Europe/Lisbon");
		startTimeList.put(content);

		JSONArray endTimeList = new JSONArray(); 
		content = new JSONObject(); 
		content.put("dateTime", end); 
		content.put("timeZone","Europe/Lisbon"); 
		endTimeList.put(content);

		jBody.put("start", startTimeList); jBody.put("end", endTimeList);
		
		request.addHeader("Content-Type", "application/json; charset=UTF-8");
		request.addPayload(jBody.toString());
		service.signRequest(accessToken, request);
		Response response = request.send();
		//response.getBody
		System.out.println("Is File Created? Lets see what we found...");
		System.out.println("HTTP RESPONSE: =============");
		System.out.println(response.getCode());
		System.out.println(response.getBody());

		System.out.println("END RESPONSE ===============");

	}
	
	
	
	public void updateMeetingOnGoogle(Token token,String itemName,int idItem,boolean whatToDo){
		// TODO
//				Meeting m = findMeeting(meetID);
//				System.out.println("meeting ID: "+meetID);
//				System.out.println(m.toString());
//				OAuthService service = callService();
//				
//				OAuthRequest request = new OAuthRequest(Verb.GET,
//						"https://www.googleapis.com/calendar/v3/calendars/primary/events");
//
//
//				request.addHeader("Content-Type", "application/json; charset=UTF-8");
//				request.addPayload(jBody.toString());
//				service.signRequest(accessToken, request);
//				Response response = request.send();
//				//response.getBody
//				System.out.println("Is File Created? Lets see what we found...");
//				System.out.println("HTTP RESPONSE: =============");
//				System.out.println(response.getCode());
//				System.out.println(response.getBody());
//
//				System.out.println("END RESPONSE ===============");

		
	}


}
