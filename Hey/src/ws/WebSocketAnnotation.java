package ws;

import hey.model.HeyBean;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/ws", configurator = GetBeanSessions.class)
public class WebSocketAnnotation {

	private static final Set<WebSocketAnnotation> users = new CopyOnWriteArraySet<>();
	private String username;
	private Session wsSession;
	private HttpSession httpSession;
	private HeyBean myBean;
	private int itemID;
	

	@OnOpen
	public void open(Session session, EndpointConfig config) throws IOException {
		this.wsSession = session;
		this.httpSession = (HttpSession) config.getUserProperties().get(
				HttpSession.class.getName());
		
		this.itemID = (int) httpSession.getAttribute("itemID");
		this.myBean = (HeyBean) httpSession.getAttribute("heyBean");
		this.username = (String) httpSession.getAttribute("username");
		users.add(this);

		String message = "*" + username + "*connected.";
		sendMessage(message);
	}

	@OnClose
	public void end() throws IOException {
		// clean up once the WebSocket connection is closed
		users.remove(this);
		String message = "*" + username + "* disconnected.";
		sendMessage(message);
		this.wsSession.close();
	}

	@OnMessage
	public void receiveMessage(String message) throws RemoteException {
		// one should never trust the client, and sensitive HTML
		// characters should be replaced with &lt; &gt; &quot; &amp;
		String msg = new StringBuffer(message).toString();
		msg = "[" + username + "] " + msg;
		myBean.addChatMessage(msg,itemID);
		sendMessage(msg);
	}

	
	private void sendMessage(String text) {
		// uses *this* object's session to call sendText()
		for (WebSocketAnnotation client : users) {
			try {
				synchronized (client) {
					client.wsSession.getBasicRemote().sendText(text);
				}
			} catch (IOException e) {
				users.remove(client);
				try {
					client.wsSession.close();
				} catch (IOException e1) {
					// Ignore
				}
				String message = String.format("* %s %s", client.username,
						" has   been   disconnected .");
				sendMessage(message);
			}
		}
	}

}

//@ServerEndpoint(value = "/ws")
//public class WebSocketAnnotation {
//	private static final Set<WebSocketAnnotation> users = new CopyOnWriteArraySet<>();
//	private static final AtomicInteger sequence = new AtomicInteger(1);
//	private final String username;
//	private Session session;
//
//	public WebSocketAnnotation() {
//		username = "nhe";
//	}
//
//	@OnOpen
//	public void start(Session sess) {
//		this.session = sess;
//		users.add(this);
//		String message = "*" + username + "* connected.";
//		sendMessage(message);
//	}
//
//	@OnClose
//	public void end() throws IOException {
//		// clean up once the WebSocket connection is closed
//		users.remove(this);
//		String message = "*" + username + "* disconnected.";
//		sendMessage(message);
//		this.session.close();
//	}
//
//	@OnMessage
//	public void receiveMessage(String message) {
//		// one should never trust the client, and sensitive HTML
//		// characters should be replaced with &lt; &gt; &quot; &amp;
//		String reversedMessage = new StringBuffer(message).toString();
//		sendMessage("[" + username + "] " + reversedMessage);
//	}
//
//	@OnError
//	public void handleError(Throwable t) {
//		t.printStackTrace();
//	}
//
//	private void sendMessage(String text) {
//		// uses *this* object's session to call sendText()
//		for (WebSocketAnnotation client : users) {
//			try {
//				synchronized (client) {
//					client.session.getBasicRemote().sendText(text);
//				}
//			} catch (IOException e) {
//				users.remove(client);
//				try {
//					client.session.close();
//				} catch (IOException e1) {
//					// Ignore
//				}
//				String message = String.format("* %s %s", client.username,
//						" has   been   disconnected .");
//				sendMessage(message);
//			}
//		}
//	}
//
//}
