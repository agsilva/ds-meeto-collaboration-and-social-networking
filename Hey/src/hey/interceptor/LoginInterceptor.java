
package hey.interceptor;

import java.util.Map;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class LoginInterceptor implements Interceptor {
	private static final long serialVersionUID = 189237412378L;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		Map<String, Object> session = invocation.getInvocationContext().getSession();
		
		// this method intercepts the execution of the action and we get access
		// to the session, to the action, and to the context of this invocation
		
		if(invocation.getAction().getClass().getName().equals("hey.action.RegisterAction"))
			return invocation.invoke();
		
		if(session.get("userID") != null){
			
			return invocation.invoke();
		}
			
		else{
			System.out.println("You are not logged in!");
			return "loginRef";
		}
			
	}

	@Override
	public void init() {
		System.out.println("iniciei o loginInterceptor()");
	}
	
	@Override
	public void destroy() { 
		System.out.println("destroi o loginInterceptor()");
	}
}