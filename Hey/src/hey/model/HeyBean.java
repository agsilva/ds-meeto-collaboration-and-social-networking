package hey.model;

import java.awt.List;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import javax.websocket.Session;

import org.scribe.model.Token;

import rmiserver.RMIServerInterface;


public class HeyBean {
	private RMIServerInterface server;
	private String username; // username and password supplied by the user
	private String password;
	private String email;
	private String real;
	private String meetingItems;
	private String outcome, title, local;
	private Date endDate, startDate;
	private int meetID;
	private String meetingUsers;
	private String itemDecisions;
	private int itemID;
	public static ArrayList<String> serverAddress = new ArrayList<String>();
	private org.scribe.model.Token token;

	public HeyBean() {

		System.out.println("hi hi, I am a Bean");
		// loadProperties();
		// System.out.println("Address: " + serverAddress.get(0));
		// try {
		// // Inicia interface RMI
		// String name = "rmi://127.0.0.1:" + 1099 + "/s";
		// System.out.println("Address: " + serverAddress.get(0));
		// server = (RMIServerInterface) Naming.lookup(name);
		// System.out.println("RMI ON");
		//
		//
		// } catch (Exception e) {
		// System.out.println("Error connecting to RMI");
		// }

		try {
			// INICIA RMI NO MESMO PC
			server = (RMIServerInterface) Naming.lookup("server");
		} catch (NotBoundException | MalformedURLException | RemoteException e) {
			System.out.println("Problema ao ligar ao RMI");
			// e.printStackTrace(); // what happens *after* we reach this line?
		}
	}

	private static void loadProperties() {

		InputStream in = null;
		try {
			Properties properties = new Properties();
			in = new FileInputStream("app.properties");
			properties.load(in);

			serverAddress.add(properties.getProperty("server1.address"));
			serverAddress.add(properties.getProperty("server2.address"));
			System.out.println("server address1: " + serverAddress.get(0));
			System.out.println("server address2: " + serverAddress.get(1));

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Something went wrong:  loadProperties(): " + e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}

	}

	public String addKeyDecision(int itemID, String decision)
			throws RemoteException {
		int userID = server.getuserID(this.username);
		return server.addDecision2Item(itemID, decision, userID);
	}

	public String addItem(String itemName) throws RemoteException { // Used in
																	// step2
		int userID = server.getuserID(this.username);

		// GOOGLE CALENDAR UPDATES
		String googleID = server.getUserGoogleID(userID);

		if (!googleID.isEmpty()) {
			// verifica se tem a meeting aceite
			int acceptedM = server.userOnMeeting(userID, this.meetID);
			if (acceptedM == 1) {
				System.out.println("SOU DA GOOGLE MAN - EDIT :D");
				server.updateMeetingOnGoogle(token, itemName, 1, true); // dummy
																		// values
				System.out.println("ADEUS MAN - EDIT :D");
			}

		}

		return server.addItem(this.meetID, itemName, userID); // returns ok#..
																// or err#..
	}

	public String addUser2Meeting(int userID, int meetingID)
			throws RemoteException {
		return server.addUser2Meeting(meetingID, userID);
	}

	public String printKeyDecisions(int itemID) throws RemoteException { // Atualiza
																			// a
																			// variavel
																			// itemDecisions

		String reply = server.printDecisions(itemID);
		String[] split = reply.split("#");

		if (split[0].equals("print")) {
			itemDecisions = split[1];
			return "ok#";
		}

		return "err#invalid ID";
	}

	public String invitationAccepted(int id) throws RemoteException {
		int userID = server.getuserID(this.username);

		String googleID = server.getUserGoogleID(userID);

		if (!googleID.isEmpty()) {
			System.out.println("SOU DA GOOGLE MAN :D");
			server.addMeetingOnGoogle(id, token);
			System.out.println("ADEUS MAN :D");
		}

		return server.acceptDeclineMeeting(id, userID, 1);
	}

	public String invitationRefused(int id) throws RemoteException {
		int userID = server.getuserID(this.username);
		return server.acceptDeclineMeeting(id, userID, 2);
	}

	public String addMeeting() throws RemoteException, InterruptedException { // used
																				// in
																				// step1

		System.out.println("this username: " + this.username);
		int userID = server.getuserID(this.username);

		System.out.println("Marcar uma reuniao e o ID do client �: " + userID);
		String reply = server.addMeeting(title, outcome, startDate, endDate,
				local, userID);

		String[] split = reply.split("#");
		if (split[0].equals("ok"))
			this.meetID = Integer.parseInt(split[2]); // guarda o ID da meeting

		String googleID = server.getUserGoogleID(userID);

		if (!googleID.isEmpty()) {
			System.out.println("SOU DA GOOGLE MAN :D");
			server.addMeetingOnGoogle(meetID, token);
			System.out.println("ADEUS MAN :D");

		}

		return reply;

	}

	public String assignAction(int itemID, String actionUsername,
			String actionText) throws RemoteException {

		int userID = server.getuserID(actionUsername);
		if (userID != 0) {
			String reply = server.addAction2User(itemID, userID, actionText);
			return reply;

		} else {
			return "err#Invalid Username";
		}

	}

	public String modifyItem(int itemID, String itemName)
			throws RemoteException {

		int userID = server.getuserID(this.username);

		// GOOGLE CALENDAR UPDATES
		String googleID = server.getUserGoogleID(userID);

		if (!googleID.isEmpty()) {
			// verifica se tem a meeting aceite
			int acceptedM = server.userOnMeeting(userID, this.meetID);
			if (acceptedM == 1) {
				System.out.println("SOU DA GOOGLE MAN - EDIT :D");
				server.updateMeetingOnGoogle(token, itemName, 1, true); // dummy
																		// values
				System.out.println("ADEUS MAN - EDIT :D");
			}

		}

		return server.modifyItem(itemID, itemName);
	}

	public String removeItem(int itemID) throws RemoteException {

		int userID = server.getuserID(this.username);

		// GOOGLE CALENDAR UPDATES
		String googleID = server.getUserGoogleID(userID);

		if (!googleID.isEmpty()) {
			// verifica se tem a meeting aceite
			int acceptedM = server.userOnMeeting(userID, this.meetID);
			if (acceptedM == 1) {
				System.out.println("SOU DA GOOGLE MAN - EDIT :D");
				server.updateMeetingOnGoogle(token, "", 1, true); // dummy
																	// values
				System.out.println("ADEUS MAN - EDIT :D");
			}

		}

		return server.removeItem(itemID);
	}

	public String addNewItem(int meetID, String itemText, int userID)
			throws RemoteException {

		// GOOGLE CALENDAR UPDATES
		String googleID = server.getUserGoogleID(userID);

		if (!googleID.isEmpty()) {
			// verifica se tem a meeting aceite
			int acceptedM = server.userOnMeeting(userID, this.meetID);
			if (acceptedM == 1) {
				System.out.println("SOU DA GOOGLE MAN - EDIT :D");
				server.updateMeetingOnGoogle(token, itemText, 1, true); // dummy
																		// values
				System.out.println("ADEUS MAN - EDIT :D");
			}

		}
		return server.addItem(meetID, itemText, userID);
	}

	public int registGoogle(String googleID, String username)
			throws RemoteException {
		System.out.println("going to do login with google :)");
		int id = server.loginGoogle(username, googleID);
		return id;
	}

	public String printMeetingItems(int meetingInDetail) throws RemoteException {
		String reply = server.printMeetingItems(meetingInDetail);

		String[] split = reply.split("#");

		if (split[0].equals("print")) {
			setMeetingItems(split[1]);
			return "ok#";
		}

		System.out.println("err: " + split[1]);
		return reply;

	}

	public String printMeetingUsers(int meetingInDetail) throws RemoteException {
		System.out.println("ENTREI NO BEAN");

		String reply = server.printMeetingUsers(meetingInDetail);
		System.out.println("REPLY DO BEAN: " + reply);
		String[] split = reply.split("#");
		System.out.println("hey bean split[0]: " + split[0]);
		System.out.println("hey bean split[1]: " + split[1]);
		if (split[0].equals("err")) {
			System.out.println("err: " + split[1]);
			return reply;
		}

		this.meetingUsers = split[1];
		return "ok#";
	}

	public ArrayList<String> getItemActions() throws RemoteException {

		ArrayList<String> reply = server.getItemActions(itemID);
		return reply;
	}

	public String getUserActions() {
		int userID;
		try {
			userID = server.getuserID(this.username);
			String reply = server.printUserActions(userID);
			String[] split = reply.split("#");
			System.out.println(split[1]);
			return split[1];

		} catch (RemoteException e) {
			System.out.println("problema no RMI - HeyBean - getUserActions");
			return "RMI error";
		}

	}

	public void addChatMessage(String msg, int itemID) throws RemoteException {
		System.out.println("BEAN: saving on the item ID: " + itemID);
		System.out.println("this message: " + msg);
		server.addWebChatLine(msg, itemID);
	}

	public void updateItemInfo(int itemID) {
		this.itemID = itemID; // defines the current ItemID in wich the user is
	}

	public ArrayList<String> getChatItem() throws RemoteException {

		String chat = server.printChat(itemID);
		ArrayList<String> myList = new ArrayList<String>();

		String[] resul = chat.split("#");

		if (resul[0].equals("print")) {

			String[] split = resul[1].split("\n");

			for (int i = 0; i < split.length; i++) {
				myList.add(split[i]);
				System.out.println(myList.get(i));
			}

			return myList;
		}

		myList.add("Nothing on the chat to display");

		return myList;
	}

	public ArrayList<String> getMeetingInvs() throws RemoteException {

		int userID = server.getuserID(username);
		String meetings = server.printUserMeetings(userID);
		ArrayList<String> myList = new ArrayList<String>();

		String[] resul = meetings.split("#");

		if (resul[0].equals("print")) {

			String[] split = resul[1].split("\n");

			for (int i = 0; i < split.length; i++) {
				myList.add(split[i]);
				System.out.println(myList.get(i));
			}

			return myList;
		}

		myList.add("Nothing on the chat to display");

		return myList;
	}
	
	public String addGroup(String name, int userID) throws RemoteException{
		return server.createGroup(name, userID);
	}
	
	public String getUserGroups() throws RemoteException{
		int userID = server.getuserID(this.username);
		return server.printUserGroups(userID);
	}
	
	
	public String addGroup2Meeting(int meetId,int groupID) throws RemoteException{
		return server.addGroup2Meeting(meetId, groupID);
	}
	
	public String getAllGroups() throws RemoteException{
		System.out.println("Printing all groups");
		return server.printAllGroups();
	}
	
	public String markActionDone(int actionID) throws RemoteException {
		return server.actionDone(actionID);
	}

	public String registeUser(String username, String password, String email,
			String real) throws RemoteException {
		return server.registerUser(real, email, username, password);
	}
	
	public String acceptGroup(int groupID) throws RemoteException{
		int userID = server.getuserID(this.username);
		return server.acceptDeclineGroup(groupID, userID, 1);
	}
	
	public String declineGroup(int groupID) throws RemoteException{
		int userID = server.getuserID(this.username);
		return server.acceptDeclineGroup(groupID, userID, 2);
	}

	public String getUserMatchesPassword() throws RemoteException {
		return server.loginUser(this.username, this.password);
	}
	
	public String addUser2Group(int memberID,int groupID) throws RemoteException{
		return server.addUser2Group(groupID, memberID);
	}

	public void setUserOffline(int userID) throws RemoteException {
		server.becomeOffline(userID);
	}

	public ArrayList<String> getOnlineUsers() throws RemoteException {
		return server.getOnlineUsernames();

	}

	public ArrayList<String> getAllUsersIds() throws RemoteException {
		return server.getUsersIDs();
	}

	public int getUserID(String username) throws RemoteException {
		return server.getuserID(username);
	}

	public void setOnline(int id) throws RemoteException {
		server.becomeOnline(id);
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String printMeetingItems() throws RemoteException {
		String reply = server.printMeetingItems(meetID);
		String[] split = reply.split("#");

		return split[1];
	}

	public String getUserMeetings() throws RemoteException {
		int userID = server.getuserID(this.username);
		String reply = server.printUserMeetings(userID);

		String[] split = reply.split("#");
		System.out.println(split[1]);
		return split[1];
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public ArrayList<String> getCompleteUserList() throws RemoteException {
		return server.getAllUsers();
	}

	public String getMeetingUsers() {
		return meetingUsers;
	}

	public void setMeetingUsers(String meetingUsers) {
		this.meetingUsers = meetingUsers;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReal() {
		return real;
	}

	public void setReal(String real) {
		this.real = real;
	}

	public String getOutcome() {
		return outcome;
	}

	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getMeetingItems() {
		System.out.println(this.meetingItems);
		return meetingItems;
	}

	public void setMeetingItems(String meetingItems) {
		this.meetingItems = meetingItems;
	}

	public String getItemDecisions() {
		return itemDecisions;
	}

	public void setItemDecisions(String itemDecisions) {
		this.itemDecisions = itemDecisions;
	}

	public void setToken(Token token) {
		this.token = token;
	}

}
