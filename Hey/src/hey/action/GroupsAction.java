
package hey.action;

import com.opensymphony.xwork2.ActionSupport;

import notif.NotificationsWS;

import org.apache.struts2.interceptor.SessionAware;

import java.rmi.RemoteException;
import java.util.Map;

import hey.model.HeyBean;



public class GroupsAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 4L;
	private Map<String, Object> session;
	private String titleGroup;
	private int userIdGroup;
	private int invitationID;
	private int memberID;
	private int groupID;
	
	
	public String newGroup() throws RemoteException{
		String reply = this.getHeyBean().addGroup(titleGroup,(int)session.get("userID"));
		String[] split = reply.split("#");
		
		addActionMessage(split[1]);
		return SUCCESS;			

	}
	
	public String addGroup2Meeting() throws RemoteException{
		int meetID = (int)session.get("meetID");
		System.out.println("MEET ID: "+meetID);
		String reply = this.getHeyBean().addGroup2Meeting(meetID,groupID);
		String[] split = reply.split("#");
		
		addActionMessage(split[1]);
		return SUCCESS;		
	}
	
	public String acceptGroup() throws RemoteException{
		String reply = this.getHeyBean().acceptGroup(invitationID);
		String[] split = reply.split("#");
		
		addActionMessage(split[1]);
		return SUCCESS;	
	}
	
	public String declineGroup() throws RemoteException{
		String reply = this.getHeyBean().declineGroup(invitationID);
		String[] split = reply.split("#");
		
		addActionMessage(split[1]);
		return SUCCESS;	
	}
	
	
	public String groupInvMember() throws RemoteException{
		
		String reply = this.getHeyBean().addUser2Group(memberID,groupID);
		String[] split = reply.split("#");
		
		addActionMessage(split[1]);
		return SUCCESS;	
		
	}
	
	
	
	
	
	
	/**** GETS & SETS ****/
	
	
	
	
	public String getTitleGroup() {
		return titleGroup;
	}

	public int getMemberID() {
		return memberID;
	}

	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}

	public int getGroupID() {
		return groupID;
	}

	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}

	public int getInvitationID() {
		return invitationID;
	}

	public void setInvitationID(int invitationID) {
		this.invitationID = invitationID;
	}

	public void setTitleGroup(String titleGroup) {
		this.titleGroup = titleGroup;
	}

	public int getUserIdGroup() {
		return userIdGroup;
	}

	public void setUserIdGroup(int userIdGroup) {
		this.userIdGroup = userIdGroup;
	}

	public HeyBean getHeyBean() {
		if(!session.containsKey("heyBean"))
			this.setHeyBean(new HeyBean());
		return (HeyBean) session.get("heyBean");
	}

	public void setHeyBean(HeyBean heyBean) {
		this.session.put("heyBean", heyBean);
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}
