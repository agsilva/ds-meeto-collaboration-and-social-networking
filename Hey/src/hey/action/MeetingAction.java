package hey.action;

import hey.model.HeyBean;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import notif.NotificationsWS;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

public class MeetingAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 4L;
	private Map<String, Object> session;
	private String endDate,startDate,local,outcome,title,agendaItem,meetingID;
	private int userID, meetingDetailID;
	private int addUserID;
	private String addUserToMeeting;
	private int actionDoneID;
	private int invitationID;
	
	
	public String acceptInv() throws RemoteException{
		
		String reply = this.getHeyBean().invitationAccepted(invitationID);		
		String[] split = reply.split("#");
		addActionMessage(split[1]);
		return SUCCESS;

	}
	
	public String refuseInv() throws RemoteException{
		String reply = this.getHeyBean().invitationRefused(invitationID);		
		String[] split = reply.split("#");
		addActionMessage(split[1]);
		return SUCCESS;
	}
	
	
	
	public String step1() throws ParseException{ // Title, date, etc...
		
		System.out.println("endDate: "+endDate);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date dateObj = sdf.parse(startDate);
		Date dateObj2 = sdf.parse(endDate);
		
		System.out.println("Title: "+title+" Outcome: "+outcome+" Local: "+local);
		System.out.println("endDate: "+dateObj2+" StartDate: "+dateObj);
		startSession();
		System.out.println("nhe...teste..");
		this.getHeyBean().setEndDate(dateObj2);
		this.getHeyBean().setStartDate(dateObj);
		
		
		try {
			
			
			String reply = this.getHeyBean().addMeeting();
			String[] split = reply.split("#");
			
			System.out.println("reply: "+reply);
			
			if(split[0].equals("ok")){
				meetingID = split[2];
				System.out.println("meetingID: "+meetingID);
				session.put("meetingID", meetingID);
				System.out.println("session meetingID: "+session.get("meetingID"));
				return SUCCESS;
				
			}else{
				addActionMessage(split[1]);
				return "fail";
			}
			
			
		} catch (RemoteException | InterruptedException e) {
			System.out.println("Problema no MeetingAction - step1");
			addActionMessage("Sorry...server problem >__<");
			return "fail";
		}
		
	}
	
	
	public String step2(){ // Add Agenda Items
		
		try {
			String reply = this.getHeyBean().addItem(agendaItem);
			String[] split = reply.split("#");
			
			if(split[0].equals("ok")){
				return SUCCESS;
			}else{
				addActionMessage(split[1]);
				return "fail";
			}
			
			
			
		} catch (RemoteException e) {
			System.out.println("Problema no RMI - Step2 do MeetingAction");
			addActionMessage("Error: Problem connecting to the server. Sorry :(");
			return "fail";
		}
		
	}
	
	public String step3(){ // Add users to meeting
		String reply;
		
		try {
			System.out.println("Going to add user number: "+userID+"to the meetingID: "+session.get("meetingID"));
			
			//add user invitation to the "database"
			reply = this.getHeyBean().addUser2Meeting(userID,Integer.parseInt((String)session.get("meetingID")));
			String[] split = reply.split("#");
			
			if(split[0].equals("print")){
				addActionMessage(split[1]);
				
				//Send invitation to User
				String invitation = "New meeting Invitation#"+userID; // a melhorar :)
				NotificationsWS.receiveNotification(invitation);
				
				return SUCCESS;
			}else{
				addActionMessage(split[1]);
				return "fail";
			}
			
		} catch (RemoteException e) {
			System.out.println("Problema no RMI - Step3 do MeetingAction");
			addActionMessage("Error: Problem connecting to the server. Sorry :(");
			return "fail";
		}
		
	}
	
	
	
	public String actionDone(){
		
		try {
			String reply = this.getHeyBean().markActionDone(actionDoneID);
			String[] split = reply.split("#");
			addActionMessage(split[1]);
			return SUCCESS;
			
		} catch (RemoteException e) {
			System.out.println("RMI PROBLEM - actionDone() on MeetingAction");
			addActionMessage("Error: RMI connection problem");
			return "fail";
		}
		
	}
	
	
	
	
	public String addUser() throws RemoteException{ // Add user to a meeting
		
		int id = this.getHeyBean().getUserID(addUserToMeeting);
		
		if(id != 0){
			System.out.println("Gonna add user to meeting ID: "+session.get("meetingDetail"));
			
			String reply = this.getHeyBean().addUser2Meeting(id,(int)session.get("meetingDetail"));
			String[] split = reply.split("#");
			
			//Send invitation to User
			String invitation = "New meeting Invitation#"+id; // a melhorar :)
			NotificationsWS.receiveNotification(invitation);
			
			if(split[0].equals("print")){
				addActionMessage(split[1]);
				
				//atualiza a lista de users
				reply = this.getHeyBean().printMeetingUsers((int)session.get("meetingDetail"));
				split = reply.split("#");
				
				if(split[0].equals("ok"))
					return SUCCESS;
				else{
					addActionMessage(split[1]);
					return "fail";
				}
			}else{
				addActionMessage(split[1]);
				return "fail";
			}
			
			
		}else{
			addActionMessage("Error: Invalid username");
			return "fail";
		}
		
		
	}
	
	public String meetDetails(){ // Allows user to see a certain meeting in detail
		session.put("meetingDetail", meetingDetailID);
		System.out.println("SESSION: "+session.get("meetingDetail"));
		System.out.println("var: "+meetingDetailID);
		String reply;
		
		try {
			reply = this.getHeyBean().printMeetingItems((int)session.get("meetingDetail"));
			System.out.println("REPLY: "+reply);
			String[] split = reply.split("#");
			
			if(split[0].equals("ok")){
				//Agora vai validar e mostrar os users da reuni�o
				System.out.println("ENTREI AQUI");
				reply = this.getHeyBean().printMeetingUsers((int)session.get("meetingDetail"));
				System.out.println("REPLY2: "+reply);
				split = reply.split("#");
				
				if(split[0].equals("ok")){
					return SUCCESS;
				}else{
					addActionMessage(split[1]);
					return "fail";
				}
				
				
			}else{
				addActionMessage(split[1]);
				return "fail";
			}
			
		} catch (RemoteException e) {
			System.out.println("Problema no RMI - meetDetails do MeetingAction");
			addActionMessage("Error: Problem connecting to the server. Sorry :(");
			return "fail";
		}
		
		
		
		
		
	}
	
	
	
	
	@Override
	public String execute(){
		return SUCCESS;
	}
	
	
	public void startSession(){
		
		session.put("local",local);
		session.put("title", title);
		session.put("outcome", outcome);
	
		this.getHeyBean().setLocal(local);
		this.getHeyBean().setOutcome(outcome);
		this.getHeyBean().setTitle(title);

	}
	
	/****** GETs & SETs *********/
	
	public int getInvitationID(){
		return invitationID;
	}
	
	public void setInvitationID(int invitation){
		this.invitationID = invitation;
	}
	
	public int getAddUserID() {
		return addUserID;
	}


	public int getActionDoneID() {
		return actionDoneID;
	}


	public void setActionDoneID(int actionDoneID) {
		this.actionDoneID = actionDoneID;
	}


	public String getAddUserToMeeting() {
		return addUserToMeeting;
	}


	public void setAddUserToMeeting(String addUserToMeeting) {
		this.addUserToMeeting = addUserToMeeting;
	}


	public void setAddUserID(int addUserID) {
		this.addUserID = addUserID;
	}

	public int getMeetingDetailID() {
		return meetingDetailID;
	}

	public void setMeetingDetailID(int meetingDetailID) {
		this.meetingDetailID = meetingDetailID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getMeetingID() {
		return meetingID;
	}

	public void setMeetingID(String meetingID) {
		this.meetingID = meetingID;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getOutcome() {
		return outcome;
	}

	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Map<String, Object> getSession() {
		return session;
	}
	

	public String getAgendaItem() {
		return agendaItem;
	}


	public void setAgendaItem(String agendaItem) {
		this.agendaItem = agendaItem;
	}


	public HeyBean getHeyBean() {
		if(!session.containsKey("heyBean")){
			System.out.println("going to create a new bean :3");
			this.setHeyBean(new HeyBean());
		}
			
		return (HeyBean) session.get("heyBean");
	}

	public void setHeyBean(HeyBean heyBean) {
		this.session.put("heyBean", heyBean);
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
