package hey.action;

import hey.model.HeyBean;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jdt.internal.compiler.ast.TypeReference;
import org.json.*;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.GoogleApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 4L;
	private Map<String, Object> session;
	private String username = null, password = null;
	private int userID;
	private String code;

	private static final String NETWORK_NAME = "Google";
	private static final String AUTHORIZE_URL = "https://www.google.com/accounts/OAuthAuthorizeToken?oauth_token=";
	private static final String PROTECTED_RESOURCE_URL = "https://www.googleapis.com/calendar/v3/calendars/primary/events/";
	private static final String SCOPE = "https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/calendar  https://www.googleapis.com/auth/userinfo.email";
	private static final Token EMPTY_TOKEN = null;
	private static final String CALLBACK_URL = "http://localhost:8082/hey/google";

	@Override
	public String execute() { // login
		// any username is accepted without confirmation (should check using
		// RMI)
		if (this.username != null && !username.equals("")) {
			this.getHeyBean().setUsername(this.username);
			this.getHeyBean().setPassword(this.password);
			session.put("username", username);
			session.put("password", password);

			String reply;
			try {
				reply = getHeyBean().getUserMatchesPassword();
				String[] split = reply.split("#");

				if (split[0].equals("ok")) {

					userID = getHeyBean().getUserID(username);
					session.put("userID", userID);
					getHeyBean().setOnline(userID);
					return SUCCESS;
				} else {
					addActionMessage(split[1]);
					session.remove("username");
					session.remove("password");
					return LOGIN;
				}

			} catch (RemoteException e) {

				System.out.println("Problema no RMI - LoginAction");
				addActionMessage("Error: Problem connecting to the servrer. Sorry :(");
				return LOGIN;
			}

		} else
			return LOGIN;
	}

	public String loginGoogle() throws RemoteException {
		System.out.println("code: " + code);

		OAuthService service = callService();
		Verifier verifier = new Verifier(code);
		Token accessToken = service.getAccessToken(EMPTY_TOKEN, verifier);

		OAuthRequest request = new OAuthRequest(Verb.GET,
				"https://www.googleapis.com/plus/v1/people/me");

		request.addHeader("Content-Type", "application/json; charset=UTF-8");
		service.signRequest(accessToken, request);
		Response response = request.send();
		
		System.out.println(response.getBody());
		String nhe = response.getBody();
		JSONObject obj = new JSONObject(response.getBody());

		String name = obj.getString("displayName");
		System.out.println("Google name: "+name);
		String googleID = obj.getString("id");
		session.put("username", name);
		
		int userGoogleID = this.getHeyBean().registGoogle(googleID,name);
		session.put("userID", userGoogleID);
		this.getHeyBean().setUsername(name);
		this.getHeyBean().setToken(accessToken);

		return SUCCESS;
	}

	public OAuthService callService() {
		OAuthService service = new ServiceBuilder()
				.provider(GoogleApi.class)
				.apiKey("464058319942-qsttnc2t2srj73n2atdlkq3v1gqkepch.apps.googleusercontent.com")
				.apiSecret("QVeN2o4DGgK8AHfSyY6ElX_u").callback(CALLBACK_URL)
				.scope(SCOPE).build();

		return service;
	}

	public String logout() {
		String reply;
		try {
			System.out.println("userID: " + (int) session.get("userID"));
			getHeyBean().setUserOffline((int) session.get("userID"));
			removeSession();
			System.out.println("Logout done");
			addActionMessage("You have logged out, thank you for using our app");
			return SUCCESS;

		} catch (RemoteException e) {
			System.out
					.println("RMI problem: function Logout in LoginAction.java");
			return "logoutError";
		}

	}

	public String getGUrl()
	{
		return "https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=464058319942-qsttnc2t2srj73n2atdlkq3v1gqkepch.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A8082%2Fhey%2Fgoogle&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar";
	}

	public void removeSession() {
		session.remove("username");
		session.remove("password");
		session.remove("userID");
	}

	public void setUsername(String username) {
		this.username = username; // will you sanitize this input? maybe use a
									// prepared statement?
	}

	public void setPassword(String password) {
		this.password = password; // what about this input?
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public HeyBean getHeyBean() {
		if (!session.containsKey("heyBean"))
			this.setHeyBean(new HeyBean());
		return (HeyBean) session.get("heyBean");
	}

	public void setHeyBean(HeyBean heyBean) {
		this.session.put("heyBean", heyBean);
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}
