package hey.action;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;

import java.rmi.RemoteException;
import java.util.Map;
import hey.model.HeyBean;

public class RegisterAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 4L;
	private Map<String, Object> session;
	private String username = null, password = null, real = null, email = null;

	@Override
	public String execute() {
		//startSession();

		try {
			String reply = getHeyBean().registeUser(username, password, email,
					real);
			String[] split = reply.split("#");

			if (split[0].equals("ok")) {
				addActionMessage("SUCCESS: thank you for creating an account :)");
				return SUCCESS;

			} else {
				addActionMessage(split[1]);
				//endSession();
				return SUCCESS;
			}

		} catch (RemoteException e) {
			System.out.println("Problem no RMI - Register Action");
			addActionMessage("Error: Problem connecting to the servrer. Sorry :(");
			return SUCCESS;
		}

	}

	public void startSession() {
		this.getHeyBean().setUsername(this.username);
		this.getHeyBean().setPassword(this.password);
		this.getHeyBean().setEmail(this.email);
		this.getHeyBean().setReal(this.real);
//		session.put("username", username);
//		session.put("password", password);
//		session.put("email", email);
//		session.put("real", real);
	}

//	public void endSession() {
//		session.remove("username");
//		session.remove("password");
//		session.remove("email");
//		session.remove("real");
//	}

	public String getReal() {
		return real;
	}

	public void setReal(String real) {
		this.real = real;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setUsername(String username) {
		this.username = username; // will you sanitize this input? maybe use a
									// prepared statement?
	}

	public void setPassword(String password) {
		this.password = password; // what about this input?
	}

	public HeyBean getHeyBean() {
		if (!session.containsKey("heyBean"))
			this.setHeyBean(new HeyBean());
		return (HeyBean) session.get("heyBean");
	}

	public void setHeyBean(HeyBean heyBean) {
		this.session.put("heyBean", heyBean);
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}
