
package hey.action;

import com.opensymphony.xwork2.ActionSupport;

import notif.NotificationsWS;

import org.apache.struts2.interceptor.SessionAware;

import java.rmi.RemoteException;
import java.util.Map;

import hey.model.HeyBean;



public class ItemsAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 4L;
	private Map<String, Object> session;
	private String addItemText;
	private int ItemID;
	private String itemModifyText;
	private int itemModifyID;
	
	//Actions/taks
	private String actionUsername;
	private String actionText;
	private int actionItemID;
	
	//See item in detail
	private int itemInDetailID;
	private int keyDecisionText;
	private String decision;
	

	public String addKeyDecision() throws RemoteException{

		
		String reply = this.getHeyBean().addKeyDecision((int)session.get("itemID"),decision);
		String[] split = reply.split("#");
		
		if(split[0].equals("print")){
			this.getHeyBean().printKeyDecisions((int)session.get("itemID"));
			//atualiza a lista
			addActionMessage(split[1]);
			return SUCCESS;			
		}else{
			addActionMessage(split[1]);
			return "fail";
		}
		
	}
	
	
	
	public String itemDetails(){
		String reply;
		
		try {
			session.put("itemID",itemInDetailID);
			
			reply = this.getHeyBean().printKeyDecisions(itemInDetailID);
			String[] split = reply.split("#");
			
			if(split[0].equals("ok")){
				this.getHeyBean().updateItemInfo(itemInDetailID);
				return SUCCESS;			
			}
			addActionMessage("Invalid Item ID");
			return "fail";
			
		} catch (RemoteException e) {
			System.out.println("Problema no RMI - ItemsAction - itemDetails()");
			addActionMessage("ERROR: Rmi problem");
			return "fail";
		}
		
	}

	
	public String assignAction() throws RemoteException {
		String reply = this.getHeyBean().assignAction(actionItemID,actionUsername, actionText);
		String[] split = reply.split("#");
		int userID = this.getHeyBean().getUserID(actionUsername);
	
		if(split[0].equals("print")){
			
			//Send invitation to User
			String invitation = "New Action Assigned#"+userID; // a melhorar :)
			NotificationsWS.receiveNotification(invitation);
			
			
			addActionMessage(split[1]);
			return SUCCESS;
		}else{
			addActionMessage(split[1]);
			return "fail";
		}
		
	}

	
	
	public String addItem() throws RemoteException {
		
		String reply = this.getHeyBean().addNewItem((int)session.get("meetingDetail"),this.addItemText,(int)session.get("userID"));
		String[] split = reply.split("#");
		
		if(split[0].equals("ok")){
			//actualizes the list
			
			addActionMessage("Item added with success");
			reply = this.getHeyBean().printMeetingItems((int)session.get("meetingDetail"));
			split = reply.split("#");
			
			if(split[0].equals("ok")){
				return SUCCESS;
			}else{
				addActionMessage(split[1]);
				return "fail";
			}
			
		}else{
			addActionMessage(split[1]);
			return "fail";
		}
	}
	
	
	
	
	public String modifyItem() throws RemoteException{
		
		String reply = this.getHeyBean().modifyItem(this.itemModifyID,this.itemModifyText);
		String[] split = reply.split("#");
		
		if(split[0].equals("ok")){

			addActionMessage("Item modified with success");
			//actualizes the list
			reply = this.getHeyBean().printMeetingItems((int)session.get("meetingDetail"));
			split = reply.split("#");
			
			if(split[0].equals("ok")){
				return SUCCESS;
			}else{
				addActionMessage(split[1]);
				return "fail";
			}
			
		}else{
			addActionMessage(split[1]);
			return "fail";
		}
	
	}
	
	public String removeItem() throws RemoteException{
		String reply = this.getHeyBean().removeItem(this.ItemID);
		String[] split = reply.split("#");
		
		if(split[0].equals("ok")){
			//actualizes the list
			
			addActionMessage("Item deleted with success");
			reply = this.getHeyBean().printMeetingItems((int)session.get("meetingDetail"));
			split = reply.split("#");
			
			if(split[0].equals("ok")){
				return SUCCESS;
			}else{
				addActionMessage(split[1]);
				return "fail";
			}
			
		}else{
			addActionMessage(split[1]);
			return "fail";
		}
	}
	

	
	
	
	
	
	
	/*** GETS & SETS ****/
	
	

	public int getActionItemID() {
		return actionItemID;
	}

	public String getDecision() {
		return decision;
	}



	public void setDecision(String decision) {
		this.decision = decision;
	}



	public int getKeyDecisionText() {
		return keyDecisionText;
	}

	public void setKeyDecisionText(int keyDecisionText) {
		this.keyDecisionText = keyDecisionText;
	}

	public int getItemInDetailID() {
		return itemInDetailID;
	}

	public void setItemInDetailID(int itemInDetailID) {
		this.itemInDetailID = itemInDetailID;
	}

	public void setActionItemID(int actionItemID) {
		this.actionItemID = actionItemID;
	}

	
	public String getActionUsername() {
		return actionUsername;
	}



	public void setActionUsername(String actionUsername) {
		this.actionUsername = actionUsername;
	}



	public String getActionText() {
		return actionText;
	}



	public void setActionText(String actionText) {
		this.actionText = actionText;
	}

	
	public String getItemModifyText() {
		return itemModifyText;
	}

	public void setItemModifyText(String itemModifyText) {
		this.itemModifyText = itemModifyText;
	}

	public int getItemModifyID() {
		return itemModifyID;
	}

	public void setItemModifyID(int itemModifyID) {
		this.itemModifyID = itemModifyID;
	}

	
	public int getItemID() {
		return ItemID;
	}

	public void setItemID(int itemID) {
		ItemID = itemID;
	}

	
	public String getAddItemText() {
		return addItemText;
	}

	public void setAddItemText(String addItemText) {
		this.addItemText = addItemText;
	}

	
	
	public HeyBean getHeyBean() {
		if(!session.containsKey("heyBean"))
			this.setHeyBean(new HeyBean());
		return (HeyBean) session.get("heyBean");
	}

	public void setHeyBean(HeyBean heyBean) {
		this.session.put("heyBean", heyBean);
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}
