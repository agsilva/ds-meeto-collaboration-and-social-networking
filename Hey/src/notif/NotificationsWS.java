package notif;


import hey.model.HeyBean;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import ws.GetBeanSessions;
import ws.WebSocketAnnotation;

@ServerEndpoint(value = "/notif", configurator = GetBeanSessions.class)
public class NotificationsWS {

	private static final Set<NotificationsWS> users = new CopyOnWriteArraySet<>();
	private Session wsSession;
	private HttpSession httpSession;
	private HeyBean myBean;
	

	@OnOpen
	public void open(Session session, EndpointConfig config) throws IOException {
		this.wsSession = session;
		this.httpSession = (HttpSession) config.getUserProperties().get(
				HttpSession.class.getName());
		
		this.myBean = (HeyBean) httpSession.getAttribute("heyBean");
		System.out.println("User "+httpSession.getAttribute("username")+" is now online on the notif WS :3");
		
		users.add(this); // Add to the list off the "online users"

	}

	@OnClose
	public void end() throws IOException {
		// clean up once the WebSocket connection is closed
		users.remove(this);
		this.wsSession.close();
	}

	@OnMessage
	public static void receiveNotification(String message) throws RemoteException {

		String msg = new StringBuffer(message).toString();
		String[] split = msg.split("#");
		
		System.out.println("Recieved notification alert: will now send :)");
		sendMessage(split[0],split[1]); //Send notification to the online users
	}

	
	private static void sendMessage(String text, String userID) {
		// uses *this* object's session to call sendText()
		int userNum = Integer.parseInt(userID);
		
		for (NotificationsWS client : users) {
			try {
				synchronized (client) {
					
					if((int)client.httpSession.getAttribute("userID") == userNum){
						System.out.println("SENDING NOTIF TO USER: "+client.httpSession.getAttribute("username"));
						client.wsSession.getBasicRemote().sendText(text);
					}
						
				}
			} catch (IOException e) {
				users.remove(client);
				try {
					client.wsSession.close();
				} catch (IOException e1) {
					// Ignore :3
				}
			}
		}
	}

}

