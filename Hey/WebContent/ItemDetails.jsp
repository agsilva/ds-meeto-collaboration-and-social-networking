<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


<script>
	$(document).ready(function() {
		$('.expander').click(function() {
			$(this).parent().next().slideToggle(200);
		});
	});
</script>


<title>Add Meeting</title>

<!-- Chat CSS -->
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="style.css">


<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="css/plugins/timeline.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<c:choose>
		<c:when test="${session.userID == null}">
			<jsp:forward page="/index.jsp" />
		</c:when>
	</c:choose>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand">Welcome ${session.username}</a>
		</div>
		<!-- /.navbar-header -->

		<ul class="nav navbar-top-links navbar-right">
			<li class="dropdown"><a href="/hey/hey.jsp"><div id="notif"></div></a>
				<!-- /.dropdown-alerts --></li>
			<!-- /.dropdown -->
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
					<i class="fa fa-caret-down"></i>
			</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="#"><i class="fa fa-user fa-fw"></i> User
							Profile</a></li>
					<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
					</li>
					<li class="divider"></li>
					<li><a href="<s:url action="logout" />"><i
							class="fa fa-sign-out fa-fw"></i> Logout</a></li>
				</ul> <!-- /.dropdown-user --></li>
			<!-- /.dropdown -->
		</ul>
		<!-- /.navbar-top-links -->

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">

					<li><a class="active" href="/hey/hey.jsp"> Dashboard</a></li>
					<li><a href="#">Meetings<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="/hey/addMeeting.jsp">Create meeting</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="#">Groups<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="#">My Group</a></li>
							<li><a href="#">Edit Group</a></li>
						</ul> <!-- /.nav-second-level --></li>
				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>
		<!-- /.navbar-static-side --> </nav>

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">

					<li><a class="active" href="/hey/hey.jsp"> Dashboard</a></li>
					<li><a href="#">Meetings<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="/hey/addMeeting.jsp">Create meeting</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="#">Groups<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="#">My Group</a></li>
							<li><a href="#">Edit Group</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="/hey/invitations.jsp">Notifications<span class="fa arrow"></span></a>
						<!-- /.nav-second-level --></li>
				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>
		<!-- /.navbar-static-side --> </nav>

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Item details</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>

			<div class="row">
				<div class="col-lg-12">

					<!-- START ITEM DETAILS -->

					<div class="panel panel-default">
						<div class="panel-heading">Key decisions</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">

									<s:if test="hasActionMessages()">
										<div class="welcome">
											<s:actionmessage />
										</div>
									</s:if>

									<s:form action="addKeyDecision">
										<div class="form-group">

											<pre>
											<s:property value="heyBean.itemDecisions" />
											<br>
										</pre>

											<br> <b>Add one Key Decision:</b><br>
											<s:textfield cssClass="form-control"
												placeholder="Insert Key Decision Description"
												name="decision" style="width:50%" />
											<br>
											<s:submit value="Add Decision"
												cssClass="btn btn-primary btn-lg btn-block"
												style="width:30%" />
									</s:form>

								</div>




							</div>
							<!-- /.col-lg-6 (nested) -->
						</div>
						<!-- /.row (nested) -->
					</div>
				</div>

				<!-- END ITEM DETAILS -->
				<!-- START CHAT -->


				<div class="row">
					<div class="col-lg-12">

						<div class="panel panel-default">
							<div class="panel-heading">Item discussion</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-6">

										<div id="container">

											<div id="history">
												<c:forEach items="${heyBean.chatItem}" var="value">
													<c:out value="${value}" /><br>
												</c:forEach>
											</div>
										</div>
										<p>
											<input type="text" placeholder="type to chat" id="chat">
										</p>

									</div>
								</div>
							</div>
						</div>






						<!--  END CHAT -->
						
						
						
							<!-- START ITEM DETAILS -->

					<div class="panel panel-default">
						<div class="panel-heading">Meeting Actions</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">

									<s:if test="hasActionMessages()">
										<div class="welcome">
											<s:actionmessage />
										</div>
									</s:if>
										<div class="form-group">

											<c:forEach items="${heyBean.itemActions}" var="value">
												<c:out value="${value}" />
												<br>
											</c:forEach>

								</div>
							</div>
							<!-- /.col-lg-6 (nested) -->
						</div>
						<!-- /.row (nested) -->
					</div>
				</div>

				<!-- END ITEM DETAILS -->


					</div>
				</div>

				<!-- /.row -->
			</div>
			<!-- /#page-wrapper -->

		</div>
		<!-- /#wrapper -->

		<!-- jQuery -->
		<script src="js/jquery.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="js/plugins/metisMenu/metisMenu.min.js"></script>

		<!-- Morris Charts JavaScript -->
		<script src="js/plugins/morris/raphael.min.js"></script>
		<script src="js/plugins/morris/morris.min.js"></script>
		<script src="js/plugins/morris/morris-data.js"></script>
		
		
		<!-- Bootstrap Core JavaScript -->
		<script src="js/notif.js"></script>

		<!-- Custom Theme JavaScript -->
		<script src="js/sb-admin-2.js"></script>

		<!-- WS javascript -->
		<script src="js/ws.js"></script>
</body>

</html>
