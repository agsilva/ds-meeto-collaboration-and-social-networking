
        var websocket = null;

        window.onload = function() { // URI = ws://1:8082/hey/ws
            connect('ws://' + window.location.host + '/hey/notif');
           // document.getElementById("chat").focus();
        }

        function connect(host) { // connect to the host websocket
            if ('WebSocket' in window)
                websocket = new WebSocket(host);
            else if ('MozWebSocket' in window)
                websocket = new MozWebSocket(host);
            else {
                writeToHistory('Get a real browser which supports WebSocket.');
                return;
            }

            websocket.onopen    = onOpen; // set the event listeners below
            websocket.onclose   = onClose;
            websocket.onmessage = onMessage;
            websocket.onerror   = onError;
        }

        function onOpen(event) {
          //What to do?
        }
        
        function onClose(event) {
            //What to do?
        }
        
        function onMessage(message) { // print the received message
            writeToHistory(message.data);
        }
        
        function onError(event) {
            //What to do?
        }
        

        function writeToHistory(text) {
        	var teste = document.getElementById('teste');
            var history = document.getElementById('notif');
            var line = document.createElement('p');
            line.style.wordWrap = 'break-word';
            line.innerHTML = text;
            history.appendChild(line);
            history.scrollTop = history.scrollHeight;
            teste.appendChild("teste teste");
        }