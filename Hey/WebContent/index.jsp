<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome!</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
     <!-- Social Buttons CSS -->
    <link href="css/plugins/social-buttons.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<c:choose>
		<c:when test="${session.username != null}">
			<jsp:forward page="/hey.jsp" />
		</c:when>
	</c:choose>


    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                    <div class="panel-body">
                        <s:form action="login">  
                            <fieldset>
                                <div class="form-group">
                                	<s:textfield cssClass="form-control" placeholder="Username" name="username" />
                                </div>
                                <div class="form-group">
                                    <s:password cssClass="form-control" placeholder="Password" name="password" />
                                </div>
                                <a href ="<s:url action="nhe" />" class="btn btn-block btn-social btn-google-plus">
                               		 <i class="fa fa-google-plus"></i> Sign in with Google
                            	</a>
                                <!-- Change this to a button or input when using this as a form -->
                                <s:submit value="Login" cssClass="btn btn-lg btn-success btn-block" />
                            </fieldset>

                        </s:form>
                           	<s:if test="hasActionMessages()">
							   <div class="welcome">
							      <s:actionmessage/>
							   </div>
							</s:if>
                    </div>
                    
                </div>
                <a href="/hey/register.jsp">Create an account</a>
                 <a href="/hey/testes.jsp">Pagina de testes :D</a>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>

</body>
</html>