<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>

$(document).ready(function(){
    $('.expander').click(function(){
    	 $(this).parent().next().slideToggle(200);
});
});

</script>


<title>Add Meeting</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="css/plugins/timeline.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<c:choose>
		<c:when test="${session.userID == null}">
			<jsp:forward page="/index.jsp" />
		</c:when>
	</c:choose>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand">Welcome ${session.username}</a>
		</div>
		<!-- /.navbar-header -->

		<ul class="nav navbar-top-links navbar-right">
			<li class="dropdown"><a href="/hey/hey.jsp"><div id="notif"></div></a>
				<!-- /.dropdown-alerts --></li>
			<!-- /.dropdown -->
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
					<i class="fa fa-caret-down"></i>
			</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="#"><i class="fa fa-user fa-fw"></i> User
							Profile</a></li>
					<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
					</li>
					<li class="divider"></li>
					<li><a href="<s:url action="logout" />"><i
							class="fa fa-sign-out fa-fw"></i> Logout</a></li>
				</ul> <!-- /.dropdown-user --></li>
			<!-- /.dropdown -->
		</ul>
		<!-- /.navbar-top-links -->

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">

					<li><a class="active" href="/hey/hey.jsp"> Dashboard</a></li>
					<li><a href="#">Meetings<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="/hey/addMeeting.jsp">Create meeting</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="#">Groups<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="#">My Group</a></li>
							<li><a href="#">Edit Group</a></li>
						</ul> <!-- /.nav-second-level --></li>
				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>
		<!-- /.navbar-static-side --> </nav>

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Meeting overview</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>

			<div class="row">
				<div class="col-lg-12">
				
				<!-- START ITEM DETAILS -->
				
					<div class="panel panel-default">
						<div class="panel-heading">Items Details</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">
								
									<s:if test="hasActionMessages()">
										<div class="welcome">
											<s:actionmessage />
										</div>
									</s:if>
									
									<s:form action="itemDetails">
										<div class="form-group">
										
										<pre>
											<s:property value="heyBean.meetingItems" /><br>
										</pre>
											
											<br>
											<s:textfield cssClass="form-control" placeholder="Item ID" name="itemInDetailID" style="width:14%" />
											<br>
										<s:submit value="See details" cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
									</s:form>										
										<br>
										<h4>Other Options:</h4>
										
										<a><div class="expander">Add Item</div></a>
										
										<nhe class="text" style="display:none; width:100%">
											<s:form action="addItem">
												<s:textfield cssClass="form-control" placeholder="Isert Item here" name="addItemText" style="width:75%" /><br>
												<s:submit value="Add" cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
											</s:form>
										</nhe>
										
										<a><div class="expander">Modify Item</div></a>
										
										<nhe class="text" style="display:none; width:100%">
											<s:form action="modifyItem">
												<s:textfield cssClass="form-control" placeholder="Item ID" name="itemModifyID" style="width:30%" />
												<s:textfield cssClass="form-control" placeholder="New item title" name="itemModifyText" style="width:75%" /><br>
												<s:submit value="Modify" cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
											</s:form>
										</nhe>
										
										<a><div class="expander">Remove Item</div></a>
										
										<nhe class="text" style="display:none; width:100%">
											<s:form action ="removeItem">
												<s:textfield cssClass="form-control" placeholder="Item ID" name="itemID" style="width:14%" />
													<br>
												<s:submit value="Remove" cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
											</s:form>
										</nhe>											

										
										</div>
									
	
									
												
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
							<!-- /.row (nested) -->
						</div>
					</div>
					
						<!-- END ITEM DETAILS -->
						<!-- START USER DETAILS -->
					
		
					<div class="panel panel-default">
						<div class="panel-heading">Users Invited to the meeting</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-6">
								
									<s:if test="hasActionMessages()">
										<div class="welcome">
											<s:actionmessage />
										</div>
									</s:if>
										<div class="form-group">
											<pre><c:out value="${heyBean.meetingUsers}" /><br> </pre>
											
											
										<h4>Menu:</h4>
										<a><div class="expander">Add User to this meeting</div></a>
										<nhe class="text" style="display:none; width:100%"><br>
											<a><div class="expander">See complete users list</div></a>
											<nhe class="text" style="display:none; width:100%">				
												<p><c:forEach items="${heyBean.completeUserList}" var="value">
													<c:out value="${value}" /><br>
												</c:forEach></p>
												<br>
											</nhe>
												
											<s:form action="addUser">
												<s:textfield cssClass="form-control" placeholder="Insert Username here" name="addUserToMeeting" style="width:75%" /><br>
												<s:submit value="Add User" cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
											</s:form>
										</nhe>
										
										<a><div class="expander">Assign Task/Action to user</div></a>
										
										<nhe class="text" style="display:none; width:100%">
											<s:form action="assignAction">
												<s:textfield cssClass="form-control" placeholder="Insert Item ID" name="actionItemID" style="width:55%" /><br>
												<s:textfield cssClass="form-control" placeholder="Insert Username here" name="actionUsername" style="width:55%" /><br>
												<s:textfield cssClass="form-control" placeholder="Insert Action here" name="actionText" style="width:55%" /><br>
												<s:submit value="Assign Task" cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
											</s:form>
										</nhe>								
										
							
										<a><div class="expander">Assign Task/Action to Group</div></a>
										<nhe class="text" style="display:none; width:100%">
											<s:form action="addTaskGroup">
												<s:textfield cssClass="form-control" placeholder="Insert Group name here" name="addGroupToMeeting" style="width:75%" /><br>
												<s:submit value="Add Group" cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
											</s:form>
										</nhe>
										
									
									</div>

								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
							<!-- /.row (nested) -->
						</div>
					</div>
					<!--  END USER DETAILS -->
					
					
				</div>
			</div>

			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="js/plugins/metisMenu/metisMenu.min.js"></script>

	<!-- Morris Charts JavaScript -->
	<script src="js/plugins/morris/raphael.min.js"></script>
	<script src="js/plugins/morris/morris.min.js"></script>
	<script src="js/plugins/morris/morris-data.js"></script>
	
	
		<!-- Bootstrap Core JavaScript -->
		<script src="js/notif.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="js/sb-admin-2.js"></script>
</body>

</html>
