<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Meeto: Collaboration and Social Networking</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Social Buttons -->
    <link href="css/plugins/social-buttons.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row"  style="width:70%">
            <div class="col-md-4 col-md-offset-4" style="width:70%">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registration Form</h3>
                    </div>
                    <div class="panel-body" align="center">
                        <s:form action="register" style="width:300px">
                            <fieldset>
                            
                                <div class="form-group" align="left"> 
                                 <label>Complete Name:</label>
                                    <s:textfield cssClass="form-control" name="real" />
                                </div>
                                <div class="form-group" align="left" >
                                	<label>E-mail:</label>
                                    <s:textfield cssClass="form-control" name="email" />
                                </div>
                                
                                <div class="form-group" align="left" >
                                	<label>Username:</label>
                                    <s:textfield cssClass="form-control" name="username" />
                                </div>
                                
                                <div class="form-group" align="left" >
                                	<label>Password:</label>
                                    <s:password cssClass="form-control" name="password" />
                                </div>
                                <br>
        
                                 <s:submit value="Register" cssClass="btn btn-primary btn-lg btn-block" />

                            </fieldset>
                        </s:form>
                       <s:if test="hasActionMessages()">
						   <div class="welcome">
						      <s:actionmessage/>
						   </div>
						</s:if>
                    </div>
                    
                   
                </div>
                <a href="/hey/index.jsp"> Return to the login menu</a>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>

</body>

</html>
