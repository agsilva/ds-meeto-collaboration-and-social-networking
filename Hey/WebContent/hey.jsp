<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Meeto: Collaboration and Social Networking</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$('.expander').click(function() {
			$(this).parent().next().slideToggle(200);
		});
	});
</script>





<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="css/plugins/timeline.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">


<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<c:choose>
		<c:when test="${session.userID == null}">
			<jsp:forward page="/index.jsp" />
		</c:when>
	</c:choose>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand">Welcome ${session.username}</a>
		</div>
		<!-- /.navbar-header -->

		<ul class="nav navbar-top-links navbar-right">
			<li class="dropdown"><a href="/hey/hey.jsp"><div id="notif"></div></a>
				<!-- /.dropdown-alerts --></li>
			<!-- /.dropdown -->
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
					<i class="fa fa-caret-down"></i>
			</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="#"><i class="fa fa-user fa-fw"></i> User
							Profile</a></li>
					<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
					</li>
					<li class="divider"></li>
					<li><a href="<s:url action="logout" />"><i
							class="fa fa-sign-out fa-fw"></i> Logout</a></li>
				</ul> <!-- /.dropdown-user --></li>
			<!-- /.dropdown -->
		</ul>
		<!-- /.navbar-top-links -->

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">

					<li><a class="active" href="/hey/hey.jsp"> Dashboard</a></li>
					<li><a href="#">Meetings<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="/hey/addMeeting.jsp">Create meeting</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="#">Groups<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="/hey/createGroup.jsp">Create Group</a></li>
						</ul> <!-- /.nav-second-level --></li>
				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>
		<!-- /.navbar-static-side --> </nav>

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Dashboard</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>

			<div class="row">

				<!-- /.col-lg-8 -->
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">My upcoming meetings</div>
						<div class="panel-body">
							<s:form action="seeMeetingDetails">
								<div id="morris-donut-chart">
									<pre>
	                            		<c:out
											value="${heyBean.getUserMeetings()}" />
	                            	</pre>
								</div>
								<br>
								<s:textfield cssClass="form-control" placeholder="meeting ID"
									name="meetingDetailID" style="width:14%" />
								<br>
								<s:submit value="See the meeting in detail"
									cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
							</s:form>
							<br>
							<h4>Other Options:</h4>

							<a><div class="expander">Accept Meeting</div></a>

							<nhe class="text" style="display:none; width:100%"> <s:form
								action="meetInvYes">
								<s:textfield cssClass="form-control" placeholder="invitation ID"
									name="invitationID" style="width:75%" />
								<br>
								<s:submit value="Lets gooo! :D"
									cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
							</s:form> </nhe>

							<a><div class="expander">Decline Meeting</div></a>

							<nhe class="text" style="display:none; width:100%"> <s:form
								action="meetInvNo">
								<s:textfield cssClass="form-control" placeholder="invitation ID"
									name="invitationID" style="width:75%" />
								<br>
								<s:submit value="Sorry, maybe next time :("
									cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
							</s:form> </nhe>

							<s:if test="hasActionMessages()">
								<div class="welcome">
									<s:actionmessage />
								</div>
							</s:if>
						</div>
						<!-- /.panel-body -->
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">My to-do list</div>
						<div class="panel-body">

							<pre>
                           		 <c:out value="${heyBean.userActions}" />
                        	 </pre>

							<b>What have you done? :)</b>
							<a><div class="expander">Decline Meeting</div></a>

							<nhe class="text" style="display:none; width:100%"> <s:form
								action="meetInvNo">
								<s:textfield cssClass="form-control" placeholder="invitation ID"
									name="invitationID" style="width:75%" />
								<br>
								<s:submit value="Sorry, maybe next time :("
									cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
							</s:form> </nhe>

						</div>
						<!-- /.panel-body -->
						
						
						
						<div class="panel panel-default">
						<div class="panel-heading">My Groups</div>
						<div class="panel-body">

							<pre>
                           		 <c:out value="${heyBean.userGroups}" />
                        	 </pre>

							<b>Menu:</b>
							
							<a><div class="expander">Add user to group</div></a>

							<nhe class="text" style="display:none; width:100%">
							<c:forEach items="${heyBean.allUsersIds}" var="value">
								<c:out value="${value}" /><br>
							</c:forEach>
							
							
							<s:form action="groupInvMember">
								<s:textfield cssClass="form-control" placeholder="Group ID"
									name="groupID" style="width:75%" />
								<s:textfield cssClass="form-control" placeholder="Member ID"
									name="memberID" style="width:75%" />
								<br>
								
								
								<s:submit value="Invite user"
									cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
							</s:form></nhe>
							
							
							
							
							
							<a><div class="expander">Accept Group</div></a>

							<nhe class="text" style="display:none; width:100%">
							<s:form action="groupInvYes">
								<s:textfield cssClass="form-control" placeholder="invitation ID"
									name="invitationID" style="width:75%" />
								<br>
								<s:submit value="Accept Invite"
									cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
							</s:form> </nhe>
							
							<a><div class="expander">Decline Group</div></a>

							<nhe class="text" style="display:none; width:100%">
							<s:form action="groupInvNo">
								<s:textfield cssClass="form-control" placeholder="invitation ID"
									name="invitationID" style="width:75%" />
								<br>
								<s:submit value="Decline Invite" cssClass="btn btn-primary btn-lg btn-block" style="width:30%" />
							</s:form> </nhe>
							

						</div>
						<!-- /.panel-body -->
						
						
						
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Online Users</div>
						<div class="panel-body">
							<c:forEach items="${heyBean.onlineUsers}" var="value">
								<c:out value="${value}" />
								<br>
							</c:forEach>

						</div>
					</div>

					<!-- /.col-lg-4 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /#page-wrapper -->

		</div>
		<!-- /#wrapper -->

		<!-- jQuery -->
		<script src="js/jquery.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="js/notif.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="js/plugins/metisMenu/metisMenu.min.js"></script>

		<!-- Morris Charts JavaScript -->
		<script src="js/plugins/morris/raphael.min.js"></script>
		<script src="js/plugins/morris/morris.min.js"></script>
		<script src="js/plugins/morris/morris-data.js"></script>

		<!-- Custom Theme JavaScript -->
		<script src="js/sb-admin-2.js"></script>
</body>

</html>